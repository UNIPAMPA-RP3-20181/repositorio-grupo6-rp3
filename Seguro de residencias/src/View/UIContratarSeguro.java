/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controllers.AvaliarResController;
import Controllers.ContratarSeguroController;
import Controllers.SolicitarSeguroController;
import DAO.ContratarSeguroDAO;
import DAO.SoliSeguro_ResidenciaDAO;
import Model.ApoliceSeguro;
import Model.ContratarSeguro;
import Model.Pessoa;
import Model.Residencia;
import Model.SolicitacaoSeguro;
import java.awt.Color;
import java.lang.instrument.IllegalClassFormatException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author judso
 */
public class UIContratarSeguro extends javax.swing.JFrame {

    static UIContratarSeguro getInstance() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private DefaultListModel<ApoliceSeguro> defaultList;
    private UIMenuCliente telaCliente;

    /**
     * Creates new form ContratarSeguro
     */
    public UIContratarSeguro() {
        defaultList = new DefaultListModel();
        initComponents();
        getImages();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        proprietarioCartao = new javax.swing.JTextField();
        codigodeSeguranca = new javax.swing.JFormattedTextField();
        parcelas = new javax.swing.JComboBox<>();
        ano = new javax.swing.JComboBox<>();
        numeroCartao = new javax.swing.JFormattedTextField();
        mes = new javax.swing.JComboBox<>();
        premioApolice = new javax.swing.JFormattedTextField();
        ConfirmaContrato = new javax.swing.JButton();
        Cancelar = new javax.swing.JButton();
        atualizar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        bensAprovados = new javax.swing.JTable();
        bgContratarSeguro = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        proprietarioCartao.setBackground(new java.awt.Color(204, 204, 204));
        proprietarioCartao.setBorder(null);
        proprietarioCartao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proprietarioCartaoActionPerformed(evt);
            }
        });
        jPanel1.add(proprietarioCartao, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 550, 250, 35));

        codigodeSeguranca.setBackground(new java.awt.Color(204, 204, 204));
        codigodeSeguranca.setForeground(new java.awt.Color(0, 0, 0));
        try {
            codigodeSeguranca.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(codigodeSeguranca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 560, 250, 35));

        parcelas.setBackground(new java.awt.Color(204, 204, 204));
        parcelas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1x", "2x", "3x", "4x", "5x", "6x", "7x", "8x", "9x", "10x", "11x", "12x" }));
        parcelas.setBorder(null);
        parcelas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                parcelasActionPerformed(evt);
            }
        });
        jPanel1.add(parcelas, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 460, 250, 35));
        parcelas.getAccessibleContext().setAccessibleName("");

        ano.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ano", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", " ", " " }));
        ano.setSelectedItem(ano);
        ano.setToolTipText("");
        ano.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                anoActionPerformed(evt);
            }
        });
        jPanel1.add(ano, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 450, 80, 30));

        numeroCartao.setBackground(new java.awt.Color(204, 204, 204));
        numeroCartao.setForeground(new java.awt.Color(0, 0, 0));
        try {
            numeroCartao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-####-####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        numeroCartao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numeroCartaoActionPerformed(evt);
            }
        });
        jPanel1.add(numeroCartao, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 460, 250, 35));

        mes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "mês", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
        mes.setToolTipText("");
        mes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mesActionPerformed(evt);
            }
        });
        jPanel1.add(mes, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 450, 80, 30));

        premioApolice.setBackground(new java.awt.Color(204, 204, 204));
        premioApolice.setForeground(new java.awt.Color(0, 0, 0));
        premioApolice.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        premioApolice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                premioApoliceActionPerformed(evt);
            }
        });
        jPanel1.add(premioApolice, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 240, 160, 70));

        ConfirmaContrato.setBackground(new java.awt.Color(204, 204, 204));
        ConfirmaContrato.setForeground(new java.awt.Color(0, 0, 0));
        ConfirmaContrato.setText("Confirmar Contrato");
        ConfirmaContrato.setBorder(null);
        ConfirmaContrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfirmaContratoActionPerformed(evt);
            }
        });
        jPanel1.add(ConfirmaContrato, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 610, 250, 35));
        ConfirmaContrato.getAccessibleContext().setAccessibleName("");

        Cancelar.setBackground(new java.awt.Color(204, 204, 204));
        Cancelar.setForeground(new java.awt.Color(0, 0, 0));
        Cancelar.setText("Cancelar");
        Cancelar.setBorder(null);
        Cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelarActionPerformed(evt);
            }
        });
        jPanel1.add(Cancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 610, 250, 35));
        Cancelar.getAccessibleContext().setAccessibleName("");

        atualizar.setBackground(new java.awt.Color(204, 204, 204));
        atualizar.setForeground(new java.awt.Color(0, 0, 0));
        atualizar.setText("Atualizar");
        atualizar.setBorder(null);
        atualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atualizarActionPerformed(evt);
            }
        });
        jPanel1.add(atualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 360, 250, 35));
        atualizar.getAccessibleContext().setAccessibleName("");

        bensAprovados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Candidato", "Estado da residência", "Cidade da residência ", "CEP da residência", "Valor Simulado do seguro  "
            }
        ));
        jScrollPane1.setViewportView(bensAprovados);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 840, 140));

        bgContratarSeguro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/bg_ContratarSeguro.jpg"))); // NOI18N
        jPanel1.add(bgContratarSeguro, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1360, 700));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1360, 700));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mesActionPerformed


    }//GEN-LAST:event_mesActionPerformed

    private void anoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_anoActionPerformed


    }//GEN-LAST:event_anoActionPerformed

    private void ConfirmaContratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfirmaContratoActionPerformed
        if (validarCampos()) {

            ContratarSeguro cs = new ContratarSeguro();
            ContratarSeguroDAO dao = new ContratarSeguroDAO();

            String par = (String) parcelas.getSelectedItem();
            int qnt = Integer.parseInt(par.replace("x", ""));

            cs.setProprietarioCartao(proprietarioCartao.getText());
            cs.setCodigoSegurancaCartao(Integer.parseInt(codigodeSeguranca.getText()));
            cs.setAnoValidadeCartao(Integer.parseInt((String) ano.getSelectedItem()));
            cs.setMesValidadeCartao(Integer.parseInt((String) mes.getSelectedItem()));
            cs.setPremioApolice(Double.parseDouble(premioApolice.getText().replace(",", ".")));
            cs.setQntParcelas(qnt);
            cs.setNumeroCartao(Long.parseLong(numeroCartao.getText().replace("-", "")));

            System.out.println("AKJSDFHASJK = " + cs.toString());

            try {
                dao.creat(cs);
            } catch (SQLException ex) {
                Logger.getLogger(UIContratarSeguro.class.getName()).log(Level.SEVERE, null, ex);
            }
            limparCampos();
            dispose();

        }
    }//GEN-LAST:event_ConfirmaContratoActionPerformed

    private void CancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelarActionPerformed
        this.setVisible(false);
        telaCliente.setVisible(true);
        limparCampos();
    }//GEN-LAST:event_CancelarActionPerformed

    private void parcelasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_parcelasActionPerformed
//            if(parcelas.setSelectedItem())
    }//GEN-LAST:event_parcelasActionPerformed

    private void atualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atualizarActionPerformed
        DefaultTableModel bensAprovados = (DefaultTableModel) this.bensAprovados.getModel();
        AvaliarResController av = new AvaliarResController();
        Object[] dados;
        //dados =  av.salvarResidencia(residencia, status);

    }//GEN-LAST:event_atualizarActionPerformed

    private void numeroCartaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numeroCartaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numeroCartaoActionPerformed

    private void premioApoliceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_premioApoliceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_premioApoliceActionPerformed

    private void proprietarioCartaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proprietarioCartaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_proprietarioCartaoActionPerformed
    public boolean validarCampos() {
        if (numeroCartao.getText().trim().length() < 16) {
            return false;
            
        }
        return true;
    }

    public void limparCampos() {
        numeroCartao.setText("");
        codigodeSeguranca.setText("");
        premioApolice.setText("");
        proprietarioCartao.setText("");
        ano.setSelectedItem("");
        mes.setSelectedItem("");
        parcelas.setSelectedItem("");

    }

    public boolean validardataCartao() {
        if (ano.getSelectedItem().equals("") || mes.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "A data de validade do cartão está vazia");
            return false;
        }
        if (ano.getSelectedItem().equals("2018") && mes.getSelectedItem().equals("Janeiro") || ano.getSelectedItem().equals("2018") && mes.getSelectedItem().equals("Fevereiro")
                || ano.getSelectedItem().equals("2018") && mes.getSelectedItem().equals("Março") || ano.getSelectedItem().equals("2018") && mes.getSelectedItem().equals("Abril")
                || ano.getSelectedItem().equals("2018") && mes.getSelectedItem().equals("Maio") || ano.getSelectedItem().equals("2018") && mes.getSelectedItem().equals("junho")) {
            JOptionPane.showMessageDialog(null, "Ocartão está vencido");
            return false;

        }
        return true;

    }

    public void getImages() {
        String diretorio = System.getProperty("user.dir");
        this.bgContratarSeguro.setIcon(new ImageIcon(diretorio + "/src/imagens/bg_ContratarSeguro.jpg"));
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Cancelar;
    private javax.swing.JButton ConfirmaContrato;
    private javax.swing.JComboBox<String> ano;
    private javax.swing.JButton atualizar;
    private javax.swing.JTable bensAprovados;
    private javax.swing.JLabel bgContratarSeguro;
    private javax.swing.JFormattedTextField codigodeSeguranca;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> mes;
    private javax.swing.JFormattedTextField numeroCartao;
    private javax.swing.JComboBox<String> parcelas;
    private javax.swing.JFormattedTextField premioApolice;
    private javax.swing.JTextField proprietarioCartao;
    // End of variables declaration//GEN-END:variables

    void setPessoa(Pessoa pessoa) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
