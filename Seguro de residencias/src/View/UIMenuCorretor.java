/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Pessoa;

/**
 *
 * @author Matheus Marques
 */
public class UIMenuCorretor extends javax.swing.JFrame {

    private static UIMenuCorretor uniqueInstance = null;
    private Pessoa pessoa;
    private UIVisuSoliSeg uiVisuSoliSeg;
    private UIAvaliarRes uiAvaliarRes;

    public UIMenuCorretor() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButtonAvaliarRes = new javax.swing.JButton();
        jButtonGerenServ = new javax.swing.JButton();
        jButtonVisuSoli = new javax.swing.JButton();
        jButtonAvaSinis = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(1360, 700));

        jButtonAvaliarRes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonAvaliarRes.setText("Avaliar Residencias");
        jButtonAvaliarRes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAvaliarResActionPerformed(evt);
            }
        });

        jButtonGerenServ.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonGerenServ.setText("Gerenciar Serviços");
        jButtonGerenServ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGerenServActionPerformed(evt);
            }
        });

        jButtonVisuSoli.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonVisuSoli.setText("Visualizar Solicitações");
        jButtonVisuSoli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVisuSoliActionPerformed(evt);
            }
        });

        jButtonAvaSinis.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonAvaSinis.setText("Avaliar Sinistros");
        jButtonAvaSinis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAvaSinisActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButton1.setText("Sair");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(564, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jButtonAvaliarRes, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(582, 582, 582))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jButtonVisuSoli, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(585, 585, 585))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jButtonAvaSinis, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButtonGerenServ, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addContainerGap()))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(105, 105, 105))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(108, 108, 108)
                .addComponent(jButtonVisuSoli)
                .addGap(43, 43, 43)
                .addComponent(jButtonAvaliarRes)
                .addGap(58, 58, 58)
                .addComponent(jButtonAvaSinis)
                .addGap(55, 55, 55)
                .addComponent(jButtonGerenServ)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 217, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(44, 44, 44))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonVisuSoliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVisuSoliActionPerformed
        uiVisuSoliSeg = new UIVisuSoliSeg();
        uiVisuSoliSeg.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButtonVisuSoliActionPerformed

    private void jButtonAvaliarResActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAvaliarResActionPerformed
        uiAvaliarRes = new UIAvaliarRes();
        uiAvaliarRes.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButtonAvaliarResActionPerformed

    private void jButtonGerenServActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGerenServActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonGerenServActionPerformed

    private void jButtonAvaSinisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAvaSinisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonAvaSinisActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    public static UIMenuCorretor getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new UIMenuCorretor();
        }
        return uniqueInstance;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonAvaSinis;
    private javax.swing.JButton jButtonAvaliarRes;
    private javax.swing.JButton jButtonGerenServ;
    private javax.swing.JButton jButtonVisuSoli;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
