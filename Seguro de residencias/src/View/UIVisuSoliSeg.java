/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controllers.VisuSeguroController;
import Model.SituSolicitacao;
import Model.SolicitacaoSeguro;
import java.awt.Color;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Matheus Marques
 */
public class UIVisuSoliSeg extends javax.swing.JFrame {

    private VisuSeguroController controllerVisuSeg;
    private UIMenuCorretor menuCorretor;

    public UIVisuSoliSeg() {
        initComponents();
        atualizarListaSoliSeguro();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListSoliSeg = new javax.swing.JList<>();
        jFieldDataVisita = new javax.swing.JFormattedTextField();
        painelRecusa = new javax.swing.JScrollPane();
        jTextReprovacao = new javax.swing.JTextArea();
        motivoRecusa = new javax.swing.JLabel();
        dataVisita = new javax.swing.JLabel();
        jButtonPreAprovar = new javax.swing.JButton();
        jButtonInfoSoli = new javax.swing.JButton();
        jButtonVoltar = new javax.swing.JButton();
        jButtonRecusar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(1360, 700));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Lista de Solicitações", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 24))); // NOI18N

        jListSoliSeg.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jListSoliSeg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListSoliSegMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jListSoliSeg);

        try {
            jFieldDataVisita.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFieldDataVisita.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jFieldDataVisitaFocusLost(evt);
            }
        });

        jTextReprovacao.setColumns(20);
        jTextReprovacao.setRows(5);
        jTextReprovacao.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextReprovacaoFocusLost(evt);
            }
        });
        painelRecusa.setViewportView(jTextReprovacao);

        motivoRecusa.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        motivoRecusa.setText("Motivo da Recusa:");

        dataVisita.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        dataVisita.setText("Data da Visita:");

        jButtonPreAprovar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonPreAprovar.setText("Pré-Aprovar");
        jButtonPreAprovar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPreAprovarActionPerformed(evt);
            }
        });

        jButtonInfoSoli.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonInfoSoli.setText("Informações da Solicitação ");
        jButtonInfoSoli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInfoSoliActionPerformed(evt);
            }
        });

        jButtonVoltar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonVoltar.setText("Voltar");
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVoltarActionPerformed(evt);
            }
        });

        jButtonRecusar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonRecusar.setText("Recusar");
        jButtonRecusar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRecusarActionPerformed(evt);
            }
        });

        jButtonSalvar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(painelRecusa, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(motivoRecusa)
                    .addComponent(dataVisita)
                    .addComponent(jFieldDataVisita, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 442, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButtonInfoSoli)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonRecusar, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonPreAprovar))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(122, 122, 122))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(dataVisita)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jFieldDataVisita, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(108, 108, 108)
                        .addComponent(motivoRecusa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(painelRecusa, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonPreAprovar)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonRecusar)
                        .addComponent(jButtonInfoSoli)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonVoltar)
                    .addComponent(jButtonSalvar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jListSoliSegMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListSoliSegMouseClicked
        try {
            if (jListSoliSeg.getSelectedValue().getStatusSolicitacao().equals("Pré-Aprovado")) {
                setVisibility(true);
            } else if (jListSoliSeg.getSelectedValue().getStatusSolicitacao().equals("Recusado")) {
                setVisibility(false);
            }
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Erro! Selecione uma solicitação valida!");
        }
    }//GEN-LAST:event_jListSoliSegMouseClicked

    private void jButtonRecusarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRecusarActionPerformed
        try {
            controllerVisuSeg.recusar(jListSoliSeg);
            setVisibility(false);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Selecione uma solicitação!");
        }

    }//GEN-LAST:event_jButtonRecusarActionPerformed

    private void jButtonPreAprovarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPreAprovarActionPerformed
        try {
            controllerVisuSeg.preAprovar(jListSoliSeg);
            setVisibility(true);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Selecione uma solicitação!");
        }

    }//GEN-LAST:event_jButtonPreAprovarActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        try {
            if (validarAno(jFieldDataVisita) && jListSoliSeg.getSelectedValue().getStatusSolicitacao().equals(SituSolicitacao.PRE_APROVADO.toString())) {
                controllerVisuSeg.salvarDataVisita(jListSoliSeg, jFieldDataVisita.getText());
                JOptionPane.showMessageDialog(null, "Solicitação atualizada com sucesso");
            } else if (validarDescricao(jTextReprovacao) && jListSoliSeg.getSelectedValue().getStatusSolicitacao().equals(SituSolicitacao.RECUSADO.toString())) {
                controllerVisuSeg.salvarReprovacao(jListSoliSeg, jTextReprovacao.getText());
                JOptionPane.showMessageDialog(null, "Solicitação atualizada com sucesso");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar solicitação! Preencha todos os campos");
        }
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonInfoSoliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonInfoSoliActionPerformed
        try {
            controllerVisuSeg.mostrarResidencia(jListSoliSeg);
            this.setVisible(false);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Erro! Selecione uma solicitação valida!");
        }


    }//GEN-LAST:event_jButtonInfoSoliActionPerformed

    private void jFieldDataVisitaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jFieldDataVisitaFocusLost
        dataVisita.setVisible(false);
        jFieldDataVisita.setVisible(false);
    }//GEN-LAST:event_jFieldDataVisitaFocusLost

    private void jTextReprovacaoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextReprovacaoFocusLost
        motivoRecusa.setVisible(false);
        jTextReprovacao.setVisible(false);
        painelRecusa.setVisible(false);
    }//GEN-LAST:event_jTextReprovacaoFocusLost

    private void jButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoltarActionPerformed
        this.setVisible(false);
        menuCorretor.setVisible(true);
    }//GEN-LAST:event_jButtonVoltarActionPerformed

    private void setVisibility(boolean visibility) {
        dataVisita.setVisible(visibility);
        jFieldDataVisita.setVisible(visibility);
        motivoRecusa.setVisible(!visibility);
        jTextReprovacao.setVisible(!visibility);
        painelRecusa.setVisible(!visibility);
    }

    private void atualizarListaSoliSeguro() {
        controllerVisuSeg = new VisuSeguroController();
        controllerVisuSeg.mostrarSoliSeguro(jListSoliSeg);
        motivoRecusa.setVisible(false);
        jTextReprovacao.setVisible(false);
        dataVisita.setVisible(false);
        jFieldDataVisita.setVisible(false);
        painelRecusa.setVisible(false);
    }

    private boolean validarDescricao(JTextArea campoTexto) {
        Pattern pattern = Pattern.compile("[#$¨&*()+!*]");
        Matcher matcher = pattern.matcher(campoTexto.getText());
        if (campoTexto.getText() == null || " ".equals(campoTexto.getText()) || "".equals(campoTexto.getText())) {
            campoTexto.setBorder(BorderFactory.createLineBorder(Color.red));
            return false;
        }
        if (matcher.find()) {
            campoTexto.setBorder(BorderFactory.createLineBorder(Color.red));
            return false;
        }
        campoTexto.setBorder(BorderFactory.createLineBorder(Color.WHITE));
        return true;
    }

    private boolean validarAno(JTextField campoData) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(campoData.getText()));
            campoData.setBorder(BorderFactory.createLineBorder(Color.white));
            return true;
        } catch (ParseException e) {
            campoData.setBorder(BorderFactory.createLineBorder(Color.red));
            return false;
        }
    }

    public JList<SolicitacaoSeguro> getjListSoliSeg() {
        return this.jListSoliSeg;
    }

    public void setjListSoliSeg(JList<SolicitacaoSeguro> jListSoliSeg) {
        this.jListSoliSeg = jListSoliSeg;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel dataVisita;
    private javax.swing.JButton jButtonInfoSoli;
    private javax.swing.JButton jButtonPreAprovar;
    private javax.swing.JButton jButtonRecusar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonVoltar;
    private javax.swing.JFormattedTextField jFieldDataVisita;
    private javax.swing.JList<SolicitacaoSeguro> jListSoliSeg;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextReprovacao;
    private javax.swing.JLabel motivoRecusa;
    private javax.swing.JScrollPane painelRecusa;
    // End of variables declaration//GEN-END:variables
}
