/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Pessoa;
import javax.swing.ImageIcon;

/**
 *
 * @author judso
 */
public class UIMenuCliente extends javax.swing.JFrame {

    private UISoliSeg uiSolicitarSeguro;
    private UIContratarSeguro contratarSeguro;

    private static UIMenuCliente uniqueInstance = null;
    private Pessoa pessoa;
    private UISolicitarServico uiSolicitarServico;

    private UIMenuCliente() {

        contratarSeguro = new UIContratarSeguro();
        uiSolicitarServico = new UISolicitarServico();

        initComponents();
        getImages();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButtonSolicitarSeguro = new javax.swing.JButton();
        jButtonContratarSeguro = new javax.swing.JButton();
        jButtonRelatarSinistro = new javax.swing.JButton();
        jButtonSair = new javax.swing.JToggleButton();
        jButtonSolicitarServico = new javax.swing.JButton();
        jButtonGerenciarCadastro = new javax.swing.JButton();
        bgMenu = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Menu Principal");
        setMinimumSize(new java.awt.Dimension(730, 635));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setMinimumSize(new java.awt.Dimension(730, 630));
        jPanel1.setPreferredSize(new java.awt.Dimension(730, 630));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButtonSolicitarSeguro.setBackground(new java.awt.Color(204, 204, 204));
        jButtonSolicitarSeguro.setForeground(new java.awt.Color(0, 0, 0));
        jButtonSolicitarSeguro.setText("Solicitar Seguro");
        jButtonSolicitarSeguro.setBorder(null);
        jButtonSolicitarSeguro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSolicitarSeguroActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonSolicitarSeguro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 230, 500, 40));

        jButtonContratarSeguro.setBackground(new java.awt.Color(204, 204, 204));
        jButtonContratarSeguro.setForeground(new java.awt.Color(0, 0, 0));
        jButtonContratarSeguro.setText("Contratar Seguro");
        jButtonContratarSeguro.setBorder(null);
        jButtonContratarSeguro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonContratarSeguroActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonContratarSeguro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 310, 500, 40));

        jButtonRelatarSinistro.setBackground(new java.awt.Color(204, 204, 204));
        jButtonRelatarSinistro.setForeground(new java.awt.Color(0, 0, 0));
        jButtonRelatarSinistro.setText("Relatar Sinistro");
        jButtonRelatarSinistro.setBorder(null);
        jButtonRelatarSinistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRelatarSinistroActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonRelatarSinistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 530, 500, 40));

        jButtonSair.setBackground(new java.awt.Color(204, 204, 204));
        jButtonSair.setForeground(new java.awt.Color(0, 0, 0));
        jButtonSair.setText("Sair");
        jButtonSair.setBorder(null);
        jButtonSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSairActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonSair, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 610, 240, 40));

        jButtonSolicitarServico.setBackground(new java.awt.Color(204, 204, 204));
        jButtonSolicitarServico.setForeground(new java.awt.Color(0, 0, 0));
        jButtonSolicitarServico.setText("Solicitar Serviços");
        jButtonSolicitarServico.setBorder(null);
        jButtonSolicitarServico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSolicitarServicoActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonSolicitarServico, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 390, 500, 40));

        jButtonGerenciarCadastro.setBackground(new java.awt.Color(204, 204, 204));
        jButtonGerenciarCadastro.setForeground(new java.awt.Color(0, 0, 0));
        jButtonGerenciarCadastro.setText("Gerenciar Cadastro");
        jButtonGerenciarCadastro.setBorder(null);
        jButtonGerenciarCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGerenciarCadastroActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonGerenciarCadastro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 460, 500, 40));
        jPanel1.add(bgMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1360, 700));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1360, 700));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonGerenciarCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGerenciarCadastroActionPerformed

    }//GEN-LAST:event_jButtonGerenciarCadastroActionPerformed

    private void jButtonSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButtonSairActionPerformed

    private void jButtonContratarSeguroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonContratarSeguroActionPerformed

        contratarSeguro.setVisible(true);

    }//GEN-LAST:event_jButtonContratarSeguroActionPerformed

    private void jButtonSolicitarSeguroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSolicitarSeguroActionPerformed
        uiSolicitarSeguro = UISoliSeg.getInstance();
        uiSolicitarSeguro.setPessoa(pessoa);
        uiSolicitarSeguro.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButtonSolicitarSeguroActionPerformed

    private void jButtonRelatarSinistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRelatarSinistroActionPerformed

    }//GEN-LAST:event_jButtonRelatarSinistroActionPerformed

    private void jButtonSolicitarServicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSolicitarServicoActionPerformed
        uiSolicitarServico.setVisible(true);
    }//GEN-LAST:event_jButtonSolicitarServicoActionPerformed

    public static UIMenuCliente getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new UIMenuCliente();
        }
        return uniqueInstance;
    }

    public UISoliSeg getSolicitarSeguro() {
        return uiSolicitarSeguro;
    }

    public void setSolicitarSeguro(UISoliSeg solicitarSeguro) {
        this.uiSolicitarSeguro = solicitarSeguro;
    }

    public UISolicitarServico getUiVisuSeguro() {
        return uiSolicitarServico;
    }

    public void setUiVisuSeguro(UISolicitarServico uiVisuSeguro) {
        this.uiSolicitarServico = uiVisuSeguro;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public void getImages() {
        String diretorio = System.getProperty("user.dir");
        this.bgMenu.setIcon(new ImageIcon(diretorio + "/src/imagens/bg_menu.jpg"));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bgMenu;
    private javax.swing.JButton jButtonContratarSeguro;
    private javax.swing.JButton jButtonGerenciarCadastro;
    private javax.swing.JButton jButtonRelatarSinistro;
    private javax.swing.JToggleButton jButtonSair;
    private javax.swing.JButton jButtonSolicitarSeguro;
    private javax.swing.JButton jButtonSolicitarServico;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
