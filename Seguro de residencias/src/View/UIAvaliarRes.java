package View;

import Controllers.AvaliarResController;
import Model.Residencia;
import Model.SituSolicitacao;
import java.sql.SQLException;
import javax.swing.JList;
import javax.swing.JOptionPane;

/**
 *
 * @author Matheus Marques
 */
public class UIAvaliarRes extends javax.swing.JFrame {

    private AvaliarResController controllerAvaliarResi;
    private UIMenuCorretor menuCorretor;
    private String statusResidencia;

    public UIAvaliarRes() {
        initComponents();
        atualizarListaSoliSeguro();
    }

    private void atualizarListaSoliSeguro() {
        controllerAvaliarResi = new AvaliarResController();
        controllerAvaliarResi.getListResidencia(jListImovel);
        jLabelAlteracoes.setVisible(false);
        jTextAlteracoes.setVisible(false);
        painelAlteracoes.setVisible(false);
        jLabelRecusa.setVisible(false);
        jTextRecusa.setVisible(false);
        painelRecusa.setVisible(false);

    }

    public JList<Residencia> getjListImovel() {
        return jListImovel;
    }

    public void setjListImovel(JList<Residencia> jListImovel) {
        this.jListImovel = jListImovel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListImovel = new javax.swing.JList<>();
        jButtonEditar = new javax.swing.JButton();
        jButtonRecusar = new javax.swing.JButton();
        jButtonAprovar = new javax.swing.JButton();
        jButtonBens = new javax.swing.JButton();
        jButtonVoltar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        painelRecusa = new javax.swing.JScrollPane();
        jTextRecusa = new javax.swing.JTextArea();
        painelAlteracoes = new javax.swing.JScrollPane();
        jTextAlteracoes = new javax.swing.JTextArea();
        jLabelRecusa = new javax.swing.JLabel();
        jLabelAlteracoes = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Avaliar Residencias");

        jPanel1.setMaximumSize(new java.awt.Dimension(1360, 700));
        jPanel1.setMinimumSize(new java.awt.Dimension(1360, 700));
        jPanel1.setPreferredSize(new java.awt.Dimension(1360, 700));
        jPanel1.setRequestFocusEnabled(false);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Lista de Residencia", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 24))); // NOI18N

        jListImovel.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jListImovel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListImovelMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jListImovel);

        jButtonEditar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonEditar.setText("Editar Imovel");
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonRecusar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonRecusar.setText("Recusar");
        jButtonRecusar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRecusarActionPerformed(evt);
            }
        });

        jButtonAprovar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonAprovar.setText("Aprovar");
        jButtonAprovar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAprovarActionPerformed(evt);
            }
        });

        jButtonBens.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonBens.setText("Ver Bens");
        jButtonBens.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBensActionPerformed(evt);
            }
        });

        jButtonVoltar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonVoltar.setText("Voltar");
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVoltarActionPerformed(evt);
            }
        });

        jButtonSalvar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jTextRecusa.setColumns(20);
        jTextRecusa.setRows(5);
        painelRecusa.setViewportView(jTextRecusa);

        jTextAlteracoes.setColumns(20);
        jTextAlteracoes.setRows(5);
        jTextAlteracoes.setText("Ignore caso não haja alterações!");
        jTextAlteracoes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextAlteracoesMouseClicked(evt);
            }
        });
        painelAlteracoes.setViewportView(jTextAlteracoes);

        jLabelRecusa.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabelRecusa.setText("Motivo da recusa:");

        jLabelAlteracoes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabelAlteracoes.setText("Alterações no imovel:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(84, 84, 84)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(painelAlteracoes, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(painelRecusa, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelRecusa)
                    .addComponent(jLabelAlteracoes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 356, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 537, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonRecusar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAprovar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonBens))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonVoltar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonSalvar)))
                .addGap(112, 112, 112))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonEditar)
                            .addComponent(jButtonRecusar)
                            .addComponent(jButtonAprovar)
                            .addComponent(jButtonBens))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonVoltar)
                            .addComponent(jButtonSalvar))
                        .addGap(26, 26, 26))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jLabelRecusa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(painelRecusa, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelAlteracoes)
                        .addGap(5, 5, 5)
                        .addComponent(painelAlteracoes, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAprovarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAprovarActionPerformed
        statusResidencia = SituSolicitacao.APROVADO.toString();
    }//GEN-LAST:event_jButtonAprovarActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        controllerAvaliarResi = new AvaliarResController();
        controllerAvaliarResi.aprovarResidencia(jListImovel);
    }//GEN-LAST:event_jButtonEditarActionPerformed

    private void jButtonRecusarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRecusarActionPerformed
        controllerAvaliarResi = new AvaliarResController();
        controllerAvaliarResi.recusarResidencia(jListImovel);
    }//GEN-LAST:event_jButtonRecusarActionPerformed

    private void jButtonBensActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBensActionPerformed
        controllerAvaliarResi = new AvaliarResController();
        controllerAvaliarResi.mostrarBens(jListImovel.getSelectedValue().getBens());
        this.setVisible(false);
    }//GEN-LAST:event_jButtonBensActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        controllerAvaliarResi = new AvaliarResController();
        try {
            controllerAvaliarResi.salvarResidencia(jListImovel);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao salvar arquivo!");
        }
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoltarActionPerformed
        menuCorretor = new UIMenuCorretor();
        menuCorretor.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButtonVoltarActionPerformed

    private void jListImovelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListImovelMouseClicked
        if (statusResidencia.equals("Aprovado")) {
            setVisibility(true);

        } else if (statusResidencia.equals("Recusado")) {
            setVisibility(false);
        }
    }//GEN-LAST:event_jListImovelMouseClicked

    private void jTextAlteracoesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextAlteracoesMouseClicked
       
    }//GEN-LAST:event_jTextAlteracoesMouseClicked
   private void setVisibility(boolean visibility){
       jLabelAlteracoes.setVisible(visibility);
       jTextAlteracoes.setVisible(visibility);
       painelAlteracoes.setVisible(visibility);
       jLabelRecusa.setVisible(!visibility);
       jTextRecusa.setVisible(!visibility);
       painelRecusa.setVisible(!visibility);
   }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAprovar;
    private javax.swing.JButton jButtonBens;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonRecusar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonVoltar;
    private javax.swing.JLabel jLabelAlteracoes;
    private javax.swing.JLabel jLabelRecusa;
    private javax.swing.JList<Residencia> jListImovel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextAlteracoes;
    private javax.swing.JTextArea jTextRecusa;
    private javax.swing.JScrollPane painelAlteracoes;
    private javax.swing.JScrollPane painelRecusa;
    // End of variables declaration//GEN-END:variables
}
