/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.SoliSeguro_ResidenciaDAO;
import Model.Bem;
import Model.Residencia;
import Model.SituSolicitacao;
import Model.SolicitacaoSeguro;
import View.UIAvaliarResBens;
import java.sql.SQLException;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListModel;

/**
 *
 * @author Matheus Marques
 */
public class AvaliarResController {

    
    private DefaultListModel<Residencia> listaResidencia = new DefaultListModel<>();
    private DefaultListModel<SolicitacaoSeguro> listaSolicitacao = new DefaultListModel<>();
    
    public AvaliarResController() {
    }

    public void getListResidencia(JList<Residencia> listaImovel) {
        SoliSeguro_ResidenciaDAO solicitacaoSegDAO = new SoliSeguro_ResidenciaDAO();
        this.listaSolicitacao = solicitacaoSegDAO.getSoliSeguroList(SituSolicitacao.PRE_APROVADO.toString());
        for (int i = 0; i < solicitacaoSegDAO.getSoliSeguroList(SituSolicitacao.PRE_APROVADO.toString()).size(); i++) {
            this.listaResidencia.addElement(solicitacaoSegDAO.getSoliSeguroList(SituSolicitacao.PRE_APROVADO.toString()).get(i).getResidencia());
        }
        listaImovel.setModel(this.listaResidencia);
    }

    public void salvarResidencia(JList<Residencia> jListImovel) throws SQLException {
        SoliSeguro_ResidenciaDAO solicitacaoSegDAO = new SoliSeguro_ResidenciaDAO();
        solicitacaoSegDAO.update(listaSolicitacao.get(jListImovel.getSelectedIndex()).getResidencia(), listaSolicitacao.get(jListImovel.getSelectedIndex()).getStatusSolicitacao());
    }

    public void mostrarBens(List<Bem> bens) {
        UIAvaliarResBens bensView = UIAvaliarResBens.getInstance();
        bensView.getjListBens().setModel((ListModel<Bem>) bens);
        bensView.setVisible(true);
    }

    public void editarBem(Bem selectedValue, String descricaoBem, String valorEstimado) {
        selectedValue.setDescricaoBem(descricaoBem);
        selectedValue.setValorEstimadoBem(valorEstimado);
    }

    public void aprovarResidencia(JList<Residencia> jListImovel) {
        listaSolicitacao.get(jListImovel.getSelectedIndex()).setStatusSolicitacao(SituSolicitacao.APROVADO.toString());
        
    }

    public void recusarResidencia(JList<Residencia> jListImovel) {
        listaSolicitacao.get(jListImovel.getSelectedIndex()).setStatusSolicitacao(SituSolicitacao.RECUSADO.toString());
    }
}
