/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.PessoaDAO;
import Model.Candidato;
import Model.Corretor;
import Model.Pessoa;
import Model.Segurado;
import View.UILogin;
import View.UIMenuCliente;
import View.UIMenuCorretor;
import java.lang.instrument.IllegalClassFormatException;
import java.sql.SQLException;
import java.util.Objects;

/**
 *
 * @author Matheus Marques
 */

public class LoginController {

    public LoginController() {

    }

    public void logar(String nomeUsuario, String senha, UILogin telaLogin) throws SQLException, IllegalClassFormatException {
        PessoaDAO pessoaDAO = new PessoaDAO();
        Pessoa pessoa = pessoaDAO.read(nomeUsuario, senha);
        if (Objects.equals(pessoa.getClass(), Candidato.class) || Objects.equals(pessoa.getClass(), Segurado.class)) {
            telaLogin.setVisible(false);
            UIMenuCliente menuCliente = UIMenuCliente.getInstance();
            menuCliente.setVisible(true);
            menuCliente.setPessoa(pessoa);
        }
        if (Objects.equals(pessoa.getClass(), Corretor.class)) {
            telaLogin.setVisible(false);
            UIMenuCorretor menuCorretor = UIMenuCorretor.getInstance();
            menuCorretor.setVisible(true);
            menuCorretor.setPessoa(pessoa);
        }
    }

}
