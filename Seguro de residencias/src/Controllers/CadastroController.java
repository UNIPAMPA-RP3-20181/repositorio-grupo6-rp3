/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.Candidato;
import Model.Endereco;
import DAO.PessoaDAO;
import java.sql.SQLException;

/**
 *
 * @author Matheus Marques
 */
public class CadastroController {

    public CadastroController() {
    }

    public void cadastrar(String jFieldEmail, String jComboEstado, String jFieldCidade, String jFieldNomeRua, String jFieldNumeroCasa, String jFCep, String jFieldBairro, String jComboSexo, String jFieldNomeCompleto, String jFCPF, String jFTelefone, String jFNascimento, String jFieldNomeUsuario, String jFieldSenha) throws SQLException {
        PessoaDAO candidatoDAO = new PessoaDAO();
        Endereco endereco = new Endereco(jComboEstado, jFieldCidade, jFieldNomeRua, jFieldNumeroCasa, jFCep, jFieldBairro);
        Candidato candidato = new Candidato(jFieldEmail, jComboSexo, endereco, jFieldNomeCompleto, jFCPF, jFTelefone, jFNascimento, jFieldNomeUsuario, jFieldSenha);
        candidato.setNivelPermissao(1);
        candidatoDAO.create(candidato);

    }

}
