/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.SoliSeguro_ResidenciaDAO;
import Model.Bem;
import Model.Residencia;
import Model.SituSolicitacao;
import Model.SolicitacaoSeguro;
import View.UIVisuSoliSegBens;
import View.UIVisuSoliSegImovel;
import java.sql.SQLException;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author Matheus Marques
 */

public class VisuSeguroController {


    public VisuSeguroController() {
        
    }

    public void mostrarSoliSeguro(JList<SolicitacaoSeguro> listaSolicitacao) {
        SoliSeguro_ResidenciaDAO solicitacaoSegDAO = new SoliSeguro_ResidenciaDAO();
        listaSolicitacao.setModel(solicitacaoSegDAO.getSoliSeguroList(SituSolicitacao.PENDENTE.toString())); 
    }

    public void preAprovar(JList<SolicitacaoSeguro> listaSolicitacao) {
        listaSolicitacao.getSelectedValue().setStatusSolicitacao(SituSolicitacao.PRE_APROVADO.toString());
        listaSolicitacao.getSelectedValue().setMotivoReprovacao("");
    }

    public void recusar(JList<SolicitacaoSeguro> listaSolicitacao) {
        listaSolicitacao.getSelectedValue().setStatusSolicitacao(SituSolicitacao.RECUSADO.toString());
        listaSolicitacao.getSelectedValue().setDataVisitaResidencia("");
    }

    public void salvarDataVisita(JList<SolicitacaoSeguro> listaSolicitacao, String dataVisitaResidencia) throws SQLException {
        SoliSeguro_ResidenciaDAO solicitacaoSegDAO = new SoliSeguro_ResidenciaDAO();
        listaSolicitacao.getSelectedValue().setDataVisitaResidencia(dataVisitaResidencia);
        solicitacaoSegDAO.update(listaSolicitacao.getSelectedValue());
    }

    public void salvarReprovacao(JList<SolicitacaoSeguro> listaSolicitacao, String motivoReprovacao) throws SQLException {
        SoliSeguro_ResidenciaDAO solicitacaoSegDAO = new SoliSeguro_ResidenciaDAO();
        listaSolicitacao.getSelectedValue().setMotivoReprovacao(motivoReprovacao);
        solicitacaoSegDAO.update(listaSolicitacao.getSelectedValue());
    }

    public void mostrarResidencia(JList<SolicitacaoSeguro> listaSolicitacao) {
        UIVisuSoliSegImovel imovelView = UIVisuSoliSegImovel.getInstance();
        DefaultListModel<Residencia> residencia = new DefaultListModel();
        residencia.addElement(listaSolicitacao.getSelectedValue().getResidencia());
        imovelView.getjListImovel().setModel(residencia);
        imovelView.setVisible(true);
    }

    public void mostrarBens(JList<Residencia> listaResidencia) {
        UIVisuSoliSegBens bensView = UIVisuSoliSegBens.getInstance();
        DefaultListModel<Bem> listaBens = new DefaultListModel();
        for (int i = 0; i < listaResidencia.getSelectedValue().getBens().size(); i++) {
            listaBens.addElement(listaResidencia.getSelectedValue().getBens().get(i));
        }
        bensView.getjListBens().setModel(listaBens);
        bensView.setVisible(true);
    }
}
