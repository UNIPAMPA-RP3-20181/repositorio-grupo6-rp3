/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.SoliSeguro_ResidenciaDAO;
import Model.Bem;
import Model.Candidato;
import Model.Endereco;
import Model.Pessoa;
import Model.Residencia;
import Model.Segurado;
import Model.SituSolicitacao;
import Model.SolicitacaoSeguro;
import View.UISoliSeg;
import java.lang.instrument.IllegalClassFormatException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.MissingResourceException;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author Matheus Marques
 */
public class SolicitarSeguroController {

    public SolicitarSeguroController() {
    }

    public void solicitarSeguro(Pessoa solicitante, DefaultListModel<Bem> defaultList, String jFieldAreaConstruida, String jFieldAreaTotal, String jComboEstado, String jFieldCidade, String jFieldNomeRua, String jFieldNroCasa, String jFCEP, String jFieldBairro, String jFieldQuantidadeComodos, String jFieldQuantidadeBanheiros, String jFieldQuantidadeGaragens, String jFieldNroAndares, String jTextDescricao, String jFAnoConstrucao, String jComboATerreno, String jComboAEstrutural) throws IllegalClassFormatException, SQLException {
        BigDecimal areaConstruida = new BigDecimal(jFieldAreaConstruida);
        BigDecimal areaTotal = new BigDecimal(jFieldAreaTotal);
        List<Bem> bens = new ArrayList<>();
        SoliSeguro_ResidenciaDAO soliSeguroDAO = new SoliSeguro_ResidenciaDAO();
        Date data = new Date(System.currentTimeMillis());
        SimpleDateFormat formatarDate = new SimpleDateFormat("dd/MM/yyyy");
        String dataSolicitacao = formatarDate.format(data);
        SolicitacaoSeguro solicitacao = new SolicitacaoSeguro();
        for (int i = 0; i < defaultList.size(); i++) {
            bens.add(defaultList.get(i));
        }
        Endereco endereco = new Endereco(jComboEstado, jFieldCidade, jFieldNomeRua, jFieldNroCasa, jFCEP, jFieldBairro);
        Residencia residencia = new Residencia(Integer.parseInt(jFieldQuantidadeComodos), Integer.parseInt(jFieldQuantidadeBanheiros), Integer.parseInt(jFieldQuantidadeGaragens), areaConstruida, areaTotal, Integer.parseInt(jFieldNroAndares), jTextDescricao, jFAnoConstrucao, endereco);
        residencia.setBens(bens);
        solicitacao.setStatusSolicitacao(SituSolicitacao.PENDENTE.toString());
        solicitacao.setValorSolicitacao(new BigDecimal(calcularValor(jComboEstado, areaConstruida, Integer.parseInt(jFieldQuantidadeComodos), bens, Integer.parseInt(jComboATerreno), Integer.parseInt(jComboAEstrutural))));
        solicitacao.setValorCorrigidoSolicitacao(new BigDecimal("0"));
        solicitacao.setTerrenoPerigoso(Integer.parseInt(jComboATerreno));
        solicitacao.setEstruturaAmeacada(Integer.parseInt(jComboAEstrutural));
        solicitacao.setDataVisitaResidencia("");
        solicitacao.setMotivoReprovacao("");
        solicitacao.setLocalizacaoPerigosa(0);
        solicitacao.setAlteracoesSolicitacoes("");
        solicitacao.setDataSolicitacao(dataSolicitacao);
        solicitacao.setResidencia(residencia);
        if (solicitante.getClass() == Candidato.class) {
            solicitacao.setSolicitante((Candidato) solicitante);
        } else if (solicitante.getClass() == Candidato.class) {
            solicitacao.setSolicitante((Segurado) solicitante);
        } else {
            throw new IllegalClassFormatException();
        }
        soliSeguroDAO.create(solicitacao);
    }

    public void adicionarBem(DefaultListModel<Bem> defaultList, JList<Bem> jListBens, UISoliSeg viewSoliSeguro) throws MissingResourceException {
        defaultList.addElement(new Bem(viewSoliSeguro.getjTextDescricaoBem().getText(), viewSoliSeguro.getjFieldValorEstimado().getText()));
        jListBens.setModel(defaultList);
        if (!atualizarValor(viewSoliSeguro)) {
            throw new MissingResourceException("Valor não pode ser simulado! Preencha todos os campos", "UISolicitarSeguro", "Campo não preenchido");
        }
    }

    public void removerBem(DefaultListModel<Bem> defaultList, JList<Bem> jListBens, UISoliSeg viewSoliSeguro) throws ArrayIndexOutOfBoundsException, MissingResourceException {
        int linhaSelecionada = jListBens.getSelectedIndex();
        defaultList.remove(linhaSelecionada);
        jListBens.setModel(defaultList);
        if (!atualizarValor(viewSoliSeguro)) {
            throw new MissingResourceException("Valor não pode ser simulado! Preencha todos os campos", "UISolicitarSeguro", "Campo não preenchido");
        }
    }

    public boolean atualizarValor(UISoliSeg viewSoliSeguro) {
        try {
            BigDecimal areaConstruida = new BigDecimal(viewSoliSeguro.getjFieldAreaConstruida().getText());
            List<Bem> bens = new ArrayList<>();
            for (int i = 0; i < viewSoliSeguro.getDefaultList().getSize(); i++) {
                bens.add(viewSoliSeguro.getDefaultList().get(i));
            }
            viewSoliSeguro.getjFieldValorSimul().setText(String.valueOf(new BigDecimal(calcularValor(String.valueOf(viewSoliSeguro.getjComboEstado().getSelectedItem()), areaConstruida, Integer.parseInt(viewSoliSeguro.getjFieldQuantidadeComodos().getText()), bens, Integer.parseInt(String.valueOf(viewSoliSeguro.getjComboATerreno().getSelectedItem())), Integer.parseInt(String.valueOf(viewSoliSeguro.getjComboAEstrutural().getSelectedItem()))))));
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public String calcularValor(String estado, BigDecimal area, int comodos, List bens, int terrenoAmeacado, int estruturaAmeacada) {
        float preco;
        switch (estado) {
            case "SC":
            case "RS":
            case "PR":
                preco = ((((356.0f * (1.03f * bens.size())) * (1.03f * comodos)) * (((terrenoAmeacado * 10f) / 100f) + 1.0f)) * (((estruturaAmeacada * 10f) / 100f) + 1.0f)) + (Integer.parseInt(area.toString()) * 8.54f);
                break;
            case "SP":
            case "RJ":
            case "ES":
            case "MG":
                preco = ((((394.80f * (1.03f * bens.size())) * (1.03f * comodos)) * (((terrenoAmeacado * 10f) / 100f) + 1.0f)) * (((estruturaAmeacada * 10f) / 100f) + 1.0f)) + (Integer.parseInt(area.toString()) * 8.54f);
                break;
            case "GO":
            case "MT":
            case "DF":
            case "MS":
                preco = ((((341.55f * (1.03f * bens.size())) * (1.03f * comodos)) * (((terrenoAmeacado * 10f) / 100f) + 1.0f)) * (((estruturaAmeacada * 10f) / 100f) + 1.0f)) + (Integer.parseInt(area.toString()) * 8.54f);
                break;
            case "AC":
            case "AM":
            case "PA":
            case "RO":
            case "TO":
                preco = ((((302.00f * (1.03f * bens.size())) * (1.03f * comodos)) * (((terrenoAmeacado * 10f) / 100f) + 1.0f)) * (((estruturaAmeacada * 10f) / 100f) + 1.0f)) + (Integer.parseInt(area.toString()) * 8.54f);
                break;
            default:
                preco = ((((327.55f * (1.03f * bens.size())) * (1.03f * comodos)) * (((terrenoAmeacado * 10f) / 100f) + 1.0f)) * (((estruturaAmeacada * 10f) / 100f) + 1.0f)) + (Integer.parseInt(area.toString()) * 8.54f);
                break;
        }
        return String.valueOf(preco);
    }

}
