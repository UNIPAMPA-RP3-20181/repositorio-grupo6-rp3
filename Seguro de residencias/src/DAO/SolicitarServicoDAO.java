/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.SolicitarServico;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author judso
 */
public class SolicitarServicoDAO {

    public void creat(SolicitarServico ss) throws SQLException {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;

        stmt = con.prepareStatement("INSERT INTO solicitarSeguro (codigoApolice, dataVisita, descricaoVisita, eletricista, encanador,chaveiro)(?,?,?,?,?); ");
        stmt.setInt(1, ss.getCodigoApolice());
        stmt.setString(2, ss.getDataVisita());
        stmt.setString(3, ss.getDescricaoVisita());
        stmt.setString(4, ss.getEletricista());
        stmt.setString(5, ss.getEncanador());
        stmt.setString(6, ss.getChaveiro());

        stmt.executeUpdate();
        DAO.closeConnection(con, stmt);
    }

    public void update(SolicitarServico ss) throws SQLException {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;

        stmt = con.prepareStatement("UPDATE solicitarServico SET codigoApolice = ?, dataVisita = ?,descricaoVisita = ?, = ? eletricista = ?, encanador = ?,chaveiro = ? WHERE id = ?");
        stmt.setInt(1, ss.getCodigoApolice());
        stmt.setString(2, ss.getDataVisita());
        stmt.setString(3, ss.getDescricaoVisita());
        stmt.setString(4, ss.getEletricista());
        stmt.setString(5, ss.getEncanador());
        stmt.setString(6, ss.getChaveiro());
        
        stmt.executeUpdate();
        DAO.closeConnection(con, stmt);
    
    }
    public List<SolicitarServico> read() {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<SolicitarServico> solicitar = new ArrayList<>();   
      
        
        
        try {
            stmt = con.prepareStatement("SELECT * FROM contratarSeguro");
            rs = stmt.executeQuery();

            while (rs.next()) {
                SolicitarServico soli = new SolicitarServico();
                soli.setCodigoApolice(rs.getInt("codigoApolice"));
                soli.setDataVisita(rs.getString("dataVisita"));
                soli.setEletricista(rs.getString("eletricista"));
                soli.setEncanador(rs.getString("encanador"));
                soli.setChaveiro(rs.getString("chaveiro"));

                solicitar.add((SolicitarServico) solicitar);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ContratarSeguroDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DAO.closeConnection(con, stmt, rs);
        }
        return solicitar;
    }

}
