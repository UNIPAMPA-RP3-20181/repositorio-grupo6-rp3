/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Sinistro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author judso
 */
public class SinistroDAO {
    
     public void creat(Sinistro s) throws SQLException{
         Connection con = DAO.getConnection();
         PreparedStatement stmt = null;
         
         stmt = con.prepareStatement("INSERT INTO contratarSeguro (listaApolice, dataSinistro, descricaoSinistro,  tipoSinistro)(?,?,?,?); ");
            stmt.setString(1, s.getListaApolice());
            stmt.setString(2, s.getDataSinistro());
            stmt.setString(3, s.getDescricaoSinistro());
            stmt.setString(4, s.getTipoSinistro());
                      
            stmt.executeUpdate();
            DAO.closeConnection(con, stmt);
    }
     public void update(Sinistro s) throws SQLException{
         Connection con = DAO.getConnection();
         PreparedStatement stmt = null;
         
         stmt = con.prepareStatement("UPDATE contratarSeguro SET listaApolice = ?, dataSinistro = ?, descriçãoSinistro =?, tipoSinistro,   WHERE idSinistro = ?)(?,?,?,?); ");
            stmt.setString(1, s.getListaApolice());
            stmt.setString(2, s.getDataSinistro());
            stmt.setString(3, s.getDescricaoSinistro());
            stmt.setString(4, s.getTipoSinistro());
            stmt.setInt(7, s.getIdSinistro());
            stmt.executeUpdate();
            DAO.closeConnection(con, stmt);
    }
    
    public List<Sinistro> read(){
        Connection con = DAO.getConnection();
         PreparedStatement stmt = null;
         ResultSet rs = null;
         
         List<Sinistro> sinistros = new ArrayList<>();
         
        try {
            stmt = con.prepareStatement("SELECT * FROM Sinistro");
             rs = stmt.executeQuery();
             
             while(rs.next()){
                Sinistro s = new Sinistro();
                s.setIdSinistro(rs.getInt("idSinistro"));
                s.setListaApolice(rs.getString("listaApolice"));
                s.setDataSinistro(rs.getString("DataSinistro"));
                s.setDescricaoSinistro(rs.getString("DescricaoSinistro"));
                s.setTipoSinistro(rs.getString("TipoSinistro"));
                sinistros.add((Sinistro) s);
             }
        } catch (SQLException ex) {
            Logger.getLogger(ContratarSeguroDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally{
            DAO.closeConnection(con, stmt, rs);
        }
        return sinistros;
   }
    
    
}
    

