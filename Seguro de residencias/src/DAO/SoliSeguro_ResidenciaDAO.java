/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package DAO;

import Model.Bem;
import Model.Candidato;
import Model.Endereco;
import Model.Residencia;
import Model.Segurado;
import Model.SolicitacaoSeguro;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

/**
 *
 * @author Matheus Marques
 */
public class SoliSeguro_ResidenciaDAO {

    public SoliSeguro_ResidenciaDAO() {
    }

    public void create(SolicitacaoSeguro soliSeguro) throws SQLException {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = 0;
        int idSoliResidencia = 0;
        stmt = con.prepareStatement("SELECT idPessoa FROM Cand_Corretor_Seg WHERE cpf=" + (soliSeguro.getSolicitante().getCPF()));
        rs = stmt.executeQuery();
        while (rs.next()) {
            id = rs.getInt("idPessoa");
        }
        stmt = con.prepareStatement("INSERT INTO SoliSeg_Res(dataVisitaResidencia, valorCorigidoSolicitacao, motivoReprovacao, terrenoPerigoso, estruturaAmeacada, localizacaoPerigosa, statusSolicitacao, alteracoesSolicitacao,"
                + " dataSolicitacao, valorSolicitacao, quantComodosRes, quantBanheirosRes, quantGaragensRes, nroAndaresRes, nroCasaRes, cepRes, ruaRes, cidadeRes, estadoRes, bairroRes, "
                + "areaConstruidaRes, areaTotalRes, anoConstrucaoRes, descricaoResidencia, idPessoa) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        stmt.setString(1, soliSeguro.getDataVisitaResidencia());
        stmt.setBigDecimal(2, soliSeguro.getValorCorrigidoSolicitacao());
        stmt.setString(3, soliSeguro.getMotivoReprovacao());
        stmt.setInt(4, soliSeguro.getTerrenoPerigoso());
        stmt.setInt(5, soliSeguro.getEstruturaAmeacada());
        stmt.setInt(6, soliSeguro.getLocalizacaoPerigosa());
        stmt.setString(7, soliSeguro.getStatusSolicitacao());
        stmt.setString(8, soliSeguro.getAlteracoesSolicitacoes());
        stmt.setString(9, soliSeguro.getDataSolicitacao());
        stmt.setBigDecimal(10, soliSeguro.getValorSolicitacao());
        stmt.setInt(11, soliSeguro.getResidencia().getQuantidadeComodos());
        stmt.setInt(12, soliSeguro.getResidencia().getQuantidadeBanheiros());
        stmt.setInt(13, soliSeguro.getResidencia().getQuantidadeGaragens());
        stmt.setInt(14, soliSeguro.getResidencia().getNroAndares());
        stmt.setString(15, soliSeguro.getResidencia().getEndereco().getNroCasa());
        stmt.setString(16, soliSeguro.getResidencia().getEndereco().getNroCep());
        stmt.setString(17, soliSeguro.getResidencia().getEndereco().getNomeRua());
        stmt.setString(18, soliSeguro.getResidencia().getEndereco().getCidade());
        stmt.setString(19, soliSeguro.getResidencia().getEndereco().getEstado());
        stmt.setString(20, soliSeguro.getResidencia().getEndereco().getBairro());
        stmt.setBigDecimal(21, soliSeguro.getResidencia().getAreaConstruida());
        stmt.setBigDecimal(22, soliSeguro.getResidencia().getAreaTotal());
        stmt.setString(23, soliSeguro.getResidencia().getAnoConstrucao());
        stmt.setString(24, soliSeguro.getResidencia().getDescricaoResidencia());
        stmt.setInt(25, id);
        stmt.executeUpdate();
        stmt = con.prepareStatement("SELECT LAST_INSERT_ID()");
        rs = stmt.executeQuery();
        while (rs.next()) {
            idSoliResidencia = rs.getInt("LAST_INSERT_ID()");
        }
        for (int i = 0; i < soliSeguro.getResidencia().getBens().size(); i++) {
            stmt = con.prepareStatement("INSERT INTO bens(idSoliSeg_Res, descricaoBem, valoEstimadoBem) VALUES(?, ?, ?)");
            stmt.setInt(1, idSoliResidencia);
            stmt.setString(2, soliSeguro.getResidencia().getBens().get(i).getDescricaoBem());
            stmt.setString(3, soliSeguro.getResidencia().getBens().get(i).getValorEstimadoBem());
            stmt.executeUpdate();
        }

        DAO.closeConnection(con, stmt);

    }

    public DefaultListModel<SolicitacaoSeguro> getSoliSeguroList(String statusSolicitacao) {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        DefaultListModel<SolicitacaoSeguro> solicitacoes = new DefaultListModel<>();
        try {
            stmt = con.prepareStatement("SELECT * FROM soliSeg_Res");
            rs = stmt.executeQuery();
            while (rs.next()) {
                if (rs.getString("statusSolicitacao").equals(statusSolicitacao)) {
                    SolicitacaoSeguro solicitacao = new SolicitacaoSeguro();
                    Residencia residencia = new Residencia(rs.getInt("quantComodosRes"), rs.getInt("quantBanheirosRes"), rs.getInt("quantGaragensRes"), new BigDecimal(rs.getString("areaConstruidaRes")), new BigDecimal(rs.getString("areaTotalRes")), rs.getInt("nroAndaresRes"), rs.getString("descricaoResidencia"), rs.getString("anoConstrucaoRes"), new Endereco(rs.getString("estadoRes"), rs.getString("cidadeRes"), rs.getString("ruaRes"), rs.getString("nroCasaRes"), rs.getString("cepRes"), rs.getString("bairroRes")));
                    solicitacao.setAlteracoesSolicitacoes(rs.getString("alteracoesSolicitacao"));
                    solicitacao.setStatusSolicitacao(rs.getString("statusSolicitacao"));
                    solicitacao.setValorSolicitacao(new BigDecimal(rs.getString("valorSolicitacao")));
                    solicitacao.setValorCorrigidoSolicitacao(new BigDecimal(rs.getString("valorCorigidoSolicitacao")));
                    solicitacao.setDataSolicitacao(rs.getString("dataSolicitacao"));
                    solicitacao.setEstruturaAmeacada(rs.getInt("estruturaAmeacada"));
                    solicitacao.setLocalizacaoPerigosa(rs.getInt("localizacaoPerigosa"));
                    solicitacao.setTerrenoPerigoso(rs.getInt("terrenoPerigoso"));
                    solicitacao.setDataVisitaResidencia(rs.getString("dataVisitaResidencia"));
                    solicitacao.setMotivoReprovacao(rs.getString("motivoReprovacao"));
                    solicitacao.setResidencia(residencia);
                    solicitacao.setIdentificador(rs.getInt("idSoliSeg_Res"));
                    residencia.setIdentificador(rs.getInt("idSoliSeg_Res"));
                    solicitacoes.addElement(solicitacao);
                }

            }
            List<Bem> bens = new ArrayList<>();
            for (int i = 0; i < solicitacoes.size(); i++) {
                stmt = con.prepareStatement("SELECT * FROM bens, soliSeg_Res WHERE soliSeg_Res.idSoliSeg_Res = bens.idSoliSeg_Res and soliSeg_Res.idSoliSeg_Res=?");
                stmt.setInt(1, i + 1);
                rs = stmt.executeQuery();
                while (rs.next()) {
                    if (rs.getString("statusSolicitacao").equals(statusSolicitacao)) {
                        bens.add(new Bem(rs.getString("descricaoBem"), rs.getString("valoEstimadoBem")));
                    }
                }
                solicitacoes.get(i).getResidencia().setBens(bens);
                bens = new ArrayList<>();
            }
            int iterator = 0;
            for (int i = 0; i < solicitacoes.size(); i++) {
                stmt = con.prepareStatement("SELECT * FROM cand_corretor_seg, soliseg_res WHERE  cand_corretor_seg.idPessoa = soliseg_res.idPessoa and soliseg_res.idPessoa=?");
                stmt.setInt(1, i + 1);
                rs = stmt.executeQuery();
                while (rs.next()) {
                    if (rs.getString("statusSolicitacao").equals(statusSolicitacao)) {
                        if (rs.getInt("nivelPermissao") == 1) {
                            solicitacoes.get(iterator).setSolicitante(new Candidato(rs.getString("email"), rs.getString("sexoCandSeg"), new Endereco(rs.getString("estado"), rs.getString("cidade"), rs.getString("rua"), rs.getString("nroCasa"), rs.getString("cep"), rs.getString("bairro")), rs.getString("nome"), rs.getString("cpf"), rs.getString("telefone"), rs.getString("dataDeNascimento"), rs.getString("nomeUsuario"), rs.getString("senha")));
                        } else {
                            solicitacoes.get(iterator).setSolicitante(new Segurado(rs.getString("email"), new Endereco(rs.getString("estado"), rs.getString("cidade"), rs.getString("rua"), rs.getString("nroCasa"), rs.getString("cep"), rs.getString("bairro")), rs.getInt("numeroDeApolices"), rs.getString("sexoCandSeg"), rs.getString("nome"), rs.getString("cpf"), rs.getString("telefone"), rs.getString("dataDeNascimento"), rs.getString("nomeUsuario"), rs.getString("senha")));
                        }
                    }
                    iterator++;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(SoliSeguro_ResidenciaDAO.class.getName()).log(Level.WARNING, null, ex);

        } finally {
            DAO.closeConnection(con, stmt, rs);
        }
        return solicitacoes;
    }

    public void update(SolicitacaoSeguro soliSeguro) throws SQLException {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;
        stmt = con.prepareCall("UPDATE SoliSeg_Res SET dataVisitaResidencia = ?, motivoReprovacao = ?, statusSolicitacao = ? WHERE idSoliSeg_Res=?");
        stmt.setString(1, soliSeguro.getDataVisitaResidencia());
        stmt.setString(2, soliSeguro.getMotivoReprovacao());
        stmt.setString(3, soliSeguro.getStatusSolicitacao());
        stmt.setInt(4, soliSeguro.getIdentificador());
        stmt.execute();
    }

    public void update(Residencia residencia, String status) throws SQLException {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;
        stmt = con.prepareCall("\"UPDATE SoliSeg_Res SET statusSolicitacao, quantComodosRes, quantBanheirosRes, quantGaragensRes, nroAndaresRes, nroCasaRes,"
                + " cepRes, ruaRes, cidadeRes, estadoRes, bairroRes, "
                + "areaConstruidaRes, areaTotalRes, anoConstrucaoRes, descricaoResidencia VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) WHERE idSoliSeg_Res=?");
        stmt.setString(1, status);
        stmt.setInt(2, residencia.getQuantidadeComodos());
        stmt.setInt(3, residencia.getQuantidadeBanheiros());
        stmt.setInt(4, residencia.getQuantidadeGaragens());
        stmt.setInt(5, residencia.getNroAndares());
        stmt.setString(6, residencia.getEndereco().getNroCasa());
        stmt.setString(7, residencia.getEndereco().getNroCep());
        stmt.setString(8, residencia.getEndereco().getNomeRua());
        stmt.setString(9, residencia.getEndereco().getCidade());
        stmt.setString(10, residencia.getEndereco().getEstado());
        stmt.setString(11, residencia.getEndereco().getBairro());
        stmt.setBigDecimal(12, residencia.getAreaConstruida());
        stmt.setBigDecimal(13, residencia.getAreaTotal());
        stmt.setString(14, residencia.getAnoConstrucao());
        stmt.setString(15, residencia.getDescricaoResidencia());
        stmt.setInt(16, residencia.getIdentificador());
        stmt.execute();
        // stmt = con.prepareCall("UPDATE ") // todo
    }

}
