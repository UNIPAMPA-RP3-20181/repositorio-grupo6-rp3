/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Candidato;
import Model.Corretor;
import Model.Endereco;
import Model.Pessoa;
import Model.Segurado;
import java.lang.instrument.IllegalClassFormatException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Matheus Marques
 */
public class PessoaDAO {

    public PessoaDAO() {
    }

    public void create(Candidato candidato) throws SQLException {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;
        stmt = con.prepareStatement("INSERT INTO Cand_Corretor_Seg(nome, nomeUsuario, senha, dataDeNascimento, cpf, sexoCandSeg, cidade, bairro, estado, rua, nroCasa, telefone, email, cep, nivelPermissao) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        stmt.setString(1, candidato.getNome());
        stmt.setString(2, candidato.getNomeUsuario());
        stmt.setString(3, candidato.getSenha());
        stmt.setString(4, candidato.getDataNascimento());
        stmt.setString(5, candidato.getCPF());
        stmt.setString(6, candidato.getSexo());
        stmt.setString(7, candidato.getEndereco().getCidade());
        stmt.setString(8, candidato.getEndereco().getBairro());
        stmt.setString(9, candidato.getEndereco().getEstado());
        stmt.setString(10, candidato.getEndereco().getNomeRua());
        stmt.setString(11, candidato.getEndereco().getNroCasa());
        stmt.setString(12, candidato.getTelefone());
        stmt.setString(13, candidato.getEmail());
        stmt.setString(14, candidato.getEndereco().getNroCep());
        stmt.setInt(15, candidato.getNivelPermissao());
        stmt.execute();
        DAO.closeConnection(con, stmt);
    }

    public Pessoa read(String nomeUsuario, String senha) throws SQLException, IllegalClassFormatException {
        Connection con = DAO.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Pessoa pessoa = null;
        stmt = con.prepareStatement("SELECT * FROM Cand_Corretor_Seg WHERE nomeUsuario =? and senha=?");
        stmt.setString(1, nomeUsuario);
        stmt.setString(2, senha);
        rs = stmt.executeQuery();
        if (rs.first()) {
            switch (rs.getInt("nivelPermissao")) {
                case 1:
                    pessoa = new Candidato(rs.getString("email"), rs.getString("sexoCandSeg"), new Endereco(rs.getString("estado"), rs.getString("cidade"), rs.getString("rua"), rs.getString("nroCasa"), rs.getString("cep"), rs.getString("bairro")), rs.getString("nome"), rs.getString("cpf"), rs.getString("telefone"), rs.getString("dataDeNascimento"), rs.getString("nomeUsuario"), rs.getString("senha"));
                    pessoa.setNivelPermissao(rs.getInt("nivelPermissao"));
                    break;
                case 2:
                    pessoa = new Segurado(rs.getString("email"), new Endereco(rs.getString("estado"), rs.getString("cidade"), rs.getString("rua"), rs.getString("nroCasa"), rs.getString("cep"), rs.getString("bairro")), rs.getInt("numeroDeApolices"), rs.getString("sexoCandSeg"), rs.getString("nome"), rs.getString("cpf"), rs.getString("telefone"), rs.getString("dataDeNascimento"), rs.getString("nomeUsuario"), rs.getString("senha"));
                    pessoa.setNivelPermissao(rs.getInt("nivelPermissao"));
                    break;
                case 3:
                    pessoa = new Corretor(rs.getString("email"), rs.getString("dataContratacao"), rs.getBoolean("ativoCorretor"), new Endereco(rs.getString("estado"), rs.getString("cidade"), rs.getString("rua"), rs.getString("nroCasa"), rs.getString("cep"), rs.getString("bairro")), rs.getString("nome"), rs.getString("cpf"), rs.getString("telefone"), rs.getString("dataDeNascimento"), rs.getString("nomeUsuario"), rs.getString("senha"));
                    pessoa.setNivelPermissao(rs.getInt("nivelPermissao"));
                    break;
                default:
                    throw new IllegalClassFormatException();
            }
        }
        DAO.closeConnection(con, stmt, rs);
        return pessoa;
    }
}
