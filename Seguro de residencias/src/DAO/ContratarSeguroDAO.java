/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.ContratarSeguro;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.transform.Result;

/**
 *
 * @author judso
 */
public class ContratarSeguroDAO {
    public void creat(ContratarSeguro cs) throws SQLException{
         Connection con = DAO.getConnection();
         PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("INSERT INTO apoliceseguro (proprietarioCartao,codigoSegurancaCartao,anoValidadeCartao,	mesValidadeCartao,premioApolice,qntParcelas,numeroCartao  )VALUES(?,?,?,?,?,?,?); ");
            stmt.setString(1, cs.getProprietarioCartao());
            stmt.setString(2, String.valueOf(cs.getCodigoSegurancaCartao()));
            stmt.setInt(3, cs.getAnoValidadeCartao());
            stmt.setInt(4, cs.getMesValidadeCartao());
            stmt.setDouble(5, cs.getPremioApolice());
            stmt.setInt(6, cs.getQntParcelas());
            stmt.setLong(7, cs.getNumeroCartao());
            
            stmt.executeUpdate();
            
            JOptionPane.showMessageDialog(null,"Parabéns agora você é um segurado da SAFEZONE");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }finally{
            DAO.closeConnection(con, stmt);
            
        }
 
       
            
            
            
            
    }
//     public void update(ContratarSeguro cs) throws SQLException{
//         Connection con = DAO.getConnection();
//         PreparedStatement stmt = null;
//         
//         stmt = con.prepareStatement("UPDATE contratarSeguro SET proprietarioCartao = ?, codigoSeguracaCartao = ?, qntParcelas =?, premioApolice =?,ano =?, mes =?,   WHERE idContratarSeguro = ?)(?,?,?,?,?,?); ");
//            stmt.setString(1, cs.getProprietarioCartao());
//            stmt.setInt(2, cs.getCodigoSegurancaCartao());
//            stmt.setInt(3, cs.getAnoValidadeCartao());
//            stmt.setInt(4, cs.getMesValidadeCartao());
//            stmt.setDouble(5, cs.getPremioApolice());
//            stmt.setInt(6, cs.getQntParcelas());
//            stmt.setLong(7, cs.getNumeroCartao());
//            stmt.executeUpdate();
//            DAO.closeConnection(con, stmt);
//    }
//    
//    public List<ContratarSeguro> read(){
//        Connection con = DAO.getConnection();
//         PreparedStatement stmt = null;
//         ResultSet rs = null;
//         
//         List<ContratarSeguro> contratos = new ArrayList<>();
//         
//        try {
//            stmt = con.prepareStatement("SELECT * FROM contratarSeguro");
//             rs = stmt.executeQuery();
//             
//             while(rs.next()){
//                ContratarSeguro con_s = new ContratarSeguro();
//                con_s.setIdContratarSeguro(rs.getInt("idContratarSeguro"));
//                con_s.setNumeroCartao(rs.getLong("numeroCartao"));
//                con_s.setCodigoSeguracaCartao(rs.getInt("codigoSeguracaCartao"));
//                con_s.setPremioApolice(rs.getBigDecimal("premioApolice"));
//                con_s.setAno(rs.getInt("ano"));
//                con_s.setMes(rs.getInt("mes"));
//                contratos.add((ContratarSeguro) contratos);
//             }
//        } catch (SQLException ex) {
//            Logger.getLogger(ContratarSeguroDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } 
//        finally{
//            DAO.closeConnection(con, stmt, rs);
//        }
//        return contratos;
//   }
//    
    
}
