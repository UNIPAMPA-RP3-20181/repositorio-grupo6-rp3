/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author judso
 */
public class SolicitarServico {
    private int codigoApolice;
    private String dataVisita;
    private String descricaoVisita;
    private String eletricista;
    private String encanador;
    private String chaveiro;

    public int getCodigoApolice() {
        return codigoApolice;
    }

    public String getDataVisita() {
        return dataVisita;
    }

    public String getDescricaoVisita() {
        return descricaoVisita;
    }

    public void setDescricaoVisita(String descricao) {
        this.descricaoVisita = descricao;
    }



    public void setCodigoApolice(int codigoApolice) {
        this.codigoApolice = codigoApolice;
    }

    public void setDataVisita(String dataVisita) {
        this.dataVisita = dataVisita;
    }

    public String getEletricista() {
        return eletricista;
    }

    public String getEncanador() {
        return encanador;
    }

    public String getChaveiro() {
        return chaveiro;
    }

    public void setEletricista(String eletricista) {
        this.eletricista = eletricista;
    }

    public void setEncanador(String encanador) {
        this.encanador = encanador;
    }

    public void setChaveiro(String chaveiro) {
        this.chaveiro = chaveiro;
    }


    
    
}
