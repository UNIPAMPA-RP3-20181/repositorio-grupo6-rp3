/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Matheus Marques
 */
public enum SituSolicitacao {
    PENDENTE("Pendente"), PRE_APROVADO("Pré-Aprovado"), APROVADO("Aprovado"), RECUSADO("Recusado");
    private final String situacao;

    SituSolicitacao(String situacao) {

        this.situacao = situacao;  
  }

    @Override
    public String toString() {
        return situacao;
    }
    
}
