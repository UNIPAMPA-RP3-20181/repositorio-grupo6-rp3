/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.math.BigDecimal;

/**
 *
 * @author judso
 */
public class ContratarSeguro {

    private int idApoliceSeguro;
    private String proprietarioCartao;
    private long numeroCartao;
    private int codigoSegurancaCartao;
    private int qntParcelas;
    private Double premioApolice;
    private int anoValidadeCartao;
    private int mesValidadeCartao;

    public int getIdApoliceSeguro() {
        return idApoliceSeguro;
    }

    public String getProprietarioCartao() {
        return proprietarioCartao;
    }

    public long getNumeroCartao() {
        return numeroCartao;
    }

    public int getCodigoSegurancaCartao() {
        return codigoSegurancaCartao;
    }

    public int getQntParcelas() {
        return qntParcelas;
    }

    public Double getPremioApolice() {
        return premioApolice;
    }

    public int getAnoValidadeCartao() {
        return anoValidadeCartao;
    }

    public int getMesValidadeCartao() {
        return mesValidadeCartao;
    }

    public void setIdApoliceSeguro(int idApoliceSeguro) {
        this.idApoliceSeguro = idApoliceSeguro;
    }

    public void setProprietarioCartao(String proprietarioCartao) {
        this.proprietarioCartao = proprietarioCartao;
    }

    public void setNumeroCartao(long numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public void setCodigoSegurancaCartao(int codigoSegurancaCartao) {
        this.codigoSegurancaCartao = codigoSegurancaCartao;
    }

    public void setQntParcelas(int qntParcelas) {
        this.qntParcelas = qntParcelas;
    }

    public void setPremioApolice(Double premioApolice) {
        this.premioApolice = premioApolice;
    }

    public void setAnoValidadeCartao(int anoValidadeCartao) {
        this.anoValidadeCartao = anoValidadeCartao;
    }

    public void setMesValidadeCartao(int mesValidadeCartao) {
        this.mesValidadeCartao = mesValidadeCartao;
    }

    @Override
    public String toString() {
        return "ContratarSeguro{" + "idApoliceSeguro=" + idApoliceSeguro + ", proprietarioCartao=" + proprietarioCartao + ", numeroCartao=" + numeroCartao + ", codigoSegurancaCartao=" + codigoSegurancaCartao + ", qntParcelas=" + qntParcelas + ", premioApolice=" + premioApolice + ", anoValidadeCartao=" + anoValidadeCartao + ", mesValidadeCartao=" + mesValidadeCartao + '}';
    }
    
    
    
}
