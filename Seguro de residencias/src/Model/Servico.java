
package Model;

public class Servico {
    
    private String descricaoServico;
    private String quantidadeServico;

    public Servico(String descricaoServico, String quantidadeServico) {
        this.descricaoServico = descricaoServico;
        this.quantidadeServico = quantidadeServico;
    }

    public String getDescricaoServico() {
        return descricaoServico;
    }

    public void setDescricaoServico(String descricaoServico) {
        this.descricaoServico = descricaoServico;
    }

    public String getQuantidadeServico() {
        return quantidadeServico;
    }

    public void setQuantidadeServico(String quantidadeServico) {
        this.quantidadeServico = quantidadeServico;
    }
    
    
    
}
