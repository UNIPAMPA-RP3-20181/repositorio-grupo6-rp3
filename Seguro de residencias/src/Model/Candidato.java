package Model;


public class Candidato extends Pessoa {

    private String sexo;

    public Candidato(String email, String sexo, Endereco endereco, String nome, String CPF, String telefone, String dataNascimento, String nomeUsuario, String senha) {
        super(email, endereco, nome, CPF, telefone, dataNascimento, nomeUsuario, senha);
        this.sexo = sexo;
    }
    
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

}
