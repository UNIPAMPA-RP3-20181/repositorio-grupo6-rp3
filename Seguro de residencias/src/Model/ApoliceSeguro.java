package Model;

import java.math.BigDecimal;

public class ApoliceSeguro extends SolicitacaoSeguro {

    private int nroApolice;
    private String contratacaoApolice;
    private BigDecimal premioApolice;
    private BigDecimal valorParcela;
    private String dataPgtoParcela;
    private String quantidadeParcelas;
    private long nroCartao;
    private String bandeiraCartao, nomeCartao;
    private String vencimento;
    private int codigoSeguranca;

    public ApoliceSeguro() {
        super();

    }

    public int getNroApolice() {
        return nroApolice;
    }

    public void setNroApolice(int nroApolice) {
        this.nroApolice = nroApolice;
    }

    public String getContratacaoApolice() {
        return contratacaoApolice;
    }

    public void setContratacaoApolice(String contratacaoApolice) {
        this.contratacaoApolice = contratacaoApolice;
    }

    public BigDecimal getPremioApolice() {
        return premioApolice;
    }

    public void setPremioApolice(BigDecimal premioApolice) {
        this.premioApolice = premioApolice;
    }

    public BigDecimal getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(BigDecimal valorParcela) {
        this.valorParcela = valorParcela;
    }



    public String getDataPgtoParcela() {
        return dataPgtoParcela;
    }

    public void setDataPgtoParcela(String dataPgtoParcela) {
        this.dataPgtoParcela = dataPgtoParcela;
    }

    public String getQuantidadeParcelas() {
        return quantidadeParcelas;
    }

    public void setQuantidadeParcelas(String quantidadeParcelas) {
        this.quantidadeParcelas = quantidadeParcelas;
    }

    public long getNroCartao() {
        return nroCartao;
    }

    public void setNroCartao(long nroCartao) {
        this.nroCartao = nroCartao;
    }

    public String getBandeiraCartao() {
        return bandeiraCartao;
    }

    public void setBandeiraCartao(String bandeiraCartao) {
        this.bandeiraCartao = bandeiraCartao;
    }

    public String getNomeCartao() {
        return nomeCartao;
    }

    public void setNomeCartao(String nomeCartao) {
        this.nomeCartao = nomeCartao;
    }

    public String getVencimento() {
        return vencimento;
    }

    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }

    public int getCodigoSeguranca() {
        return codigoSeguranca;
    }

    public void setCodigoSeguranca(int codigoSeguranca) {
        this.codigoSeguranca = codigoSeguranca;
    }

}
