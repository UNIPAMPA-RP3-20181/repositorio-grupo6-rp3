
package Model;




public class Corretor extends Pessoa {
    
    private String dataContratacao;
    private boolean ativoCorretor;

    public Corretor(String email, String dataContratacao, boolean ativoCorretor, Endereco endereco, String nome, String CPF, String telefone, String dataNascimento, String nomeUsuario, String senha ) {
        super(email, endereco, nome, CPF, telefone, dataNascimento, nomeUsuario, senha); 
        this.dataContratacao = dataContratacao;
        this.ativoCorretor = ativoCorretor;
    }

    public String getDataContratacao() {
        return dataContratacao;
    }

    public void setDataContratacao(String dataContratacao) {
        this.dataContratacao = dataContratacao;
    }

    public boolean isAtivoCorretor() {
        return ativoCorretor;
    }

    public void setAtivoCorretor(boolean ativoCorretor) {
        this.ativoCorretor = ativoCorretor;
    }

    
    
}
