
package Model;

import static java.util.Collections.list;
import java.util.logging.Logger;


public class Sinistro {

    public static void add(Sinistro sinistro) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private int idSinistro;
    private String dataSinistro;
    private String descricaoSinistro;
    private float valorSinistro;
    private boolean autorizadoSinistro, parecerAvaliador;
    private String tipoSinistro;
    private String listaApolice;

    public Sinistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setListaApolice(String listaApolice) {
        this.listaApolice = listaApolice;
    }

    public String getListaApolice() {
        return listaApolice;
    }

    public Sinistro(String dataSinistro, String descricaoSinistro, float valorSinistro, boolean autorizadoSinistro, boolean parecerAvaliador) {
        this.dataSinistro = dataSinistro;
        this.descricaoSinistro = descricaoSinistro;
        this.valorSinistro = valorSinistro;
        
    }

    public int getIdSinistro() {
        return idSinistro;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public void setIdSinistro(int idSinistro) {
        this.idSinistro = idSinistro;
    }
    private static final Logger LOG = Logger.getLogger(Sinistro.class.getName());

    public String getDataSinistro() {
        return dataSinistro;
    }

    public void setDataSinistro(String dataSinistro) {
        this.dataSinistro = dataSinistro;
    }

    public void setTipoSinistro(String tipoSinistro) {
        this.tipoSinistro = tipoSinistro;
    }

    public String getTipoSinistro() {
        return tipoSinistro;
    }

    public String getDescricaoSinistro() {
        return descricaoSinistro;
    }

    public void setDescricaoSinistro(String descricaoSinistro) {
        this.descricaoSinistro = descricaoSinistro;
    }

    public float getValorSinistro() {
        return valorSinistro;
    }

    public void setValorSinistro(float valorSinistro) {
        this.valorSinistro = valorSinistro;
    }

    public boolean isAutorizadoSinistro() {
        return autorizadoSinistro;
    }

    public void setAutorizadoSinistro(boolean autorizadoSinistro) {
        this.autorizadoSinistro = autorizadoSinistro;
    }

    public boolean isParecerAvaliador() {
        return parecerAvaliador;
    }

    public void setParecerAvaliador(boolean parecerAvaliador) {
        this.parecerAvaliador = parecerAvaliador;
    }
    
    
}
