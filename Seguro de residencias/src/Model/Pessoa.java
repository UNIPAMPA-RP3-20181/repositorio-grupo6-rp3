package Model;

public abstract class Pessoa {

    private Endereco endereco;
    private String nome;
    private String CPF;
    private String telefone;
    private String dataNascimento;
    private String nomeUsuario;
    private String senha;
    private String email;
    private int nivelPermissao;

    public Pessoa(String email, Endereco endereco, String nome, String CPF, String telefone, String dataNascimento, String nomeUsuario, String senha) {
        this.email = email;
        this.nomeUsuario = nomeUsuario;
        this.senha = senha;
        this.endereco = endereco;
        this.nome = nome;
        this.CPF = CPF;
        this.telefone = telefone;
        this.dataNascimento = dataNascimento;
    }

    public int getNivelPermissao() {
        return nivelPermissao;
    }

    public void setNivelPermissao(int nivelPermissao) {
        this.nivelPermissao = nivelPermissao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco cep) {
        this.endereco = cep;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

}
