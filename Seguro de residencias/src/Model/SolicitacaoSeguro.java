package Model;

import java.math.BigDecimal;

public class SolicitacaoSeguro {

    protected int identificador;
    protected Residencia residencia;
    protected int terrenoPerigoso;
    protected int estruturaAmeacada;
    protected int localizacaoPerigosa;
    protected String statusSolicitacao;
    protected String dataSolicitacao;
    protected String dataVisitaResidencia;
    protected BigDecimal valorSolicitacao;
    protected BigDecimal valorCorrigidoSolicitacao;
    protected String motivoReprovacao, alteracoesSolicitacoes;
    protected Candidato solicitante;

    public SolicitacaoSeguro() {

    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public int getLocalizacaoPerigosa() {
        return localizacaoPerigosa;
    }

    public void setLocalizacaoPerigosa(int localizacaoPerigosa) {
        this.localizacaoPerigosa = localizacaoPerigosa;
    }

    public String getStatusSolicitacao() {
        return statusSolicitacao;
    }

    public void setStatusSolicitacao(String statusSolicitacao) {
        this.statusSolicitacao = statusSolicitacao;
    }

    public String getMotivoReprovacao() {
        return motivoReprovacao;
    }

    public void setMotivoReprovacao(String motivoReprovacao) {
        this.motivoReprovacao = motivoReprovacao;
    }

    public Residencia getResidencia() {
        return residencia;
    }

    public void setResidencia(Residencia residencia) {
        this.residencia = residencia;
    }

    public int getTerrenoPerigoso() {
        return terrenoPerigoso;
    }

    public void setTerrenoPerigoso(int terrenoPerigoso) {
        this.terrenoPerigoso = terrenoPerigoso;
    }

    public int getEstruturaAmeacada() {
        return estruturaAmeacada;
    }

    public void setEstruturaAmeacada(int estruturaAmeacada) {
        this.estruturaAmeacada = estruturaAmeacada;
    }

    public String getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(String dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public String getDataVisitaResidencia() {
        return dataVisitaResidencia;
    }

    public void setDataVisitaResidencia(String dataVisitaResidencia) {
        this.dataVisitaResidencia = dataVisitaResidencia;
    }

    public BigDecimal getValorSolicitacao() {
        return valorSolicitacao;
    }

    public void setValorSolicitacao(BigDecimal valorSolicitacao) {
        this.valorSolicitacao = valorSolicitacao;
    }

    public BigDecimal getValorCorrigidoSolicitacao() {
        return valorCorrigidoSolicitacao;
    }

    public void setValorCorrigidoSolicitacao(BigDecimal valorCorrigidoSolicitacao) {
        this.valorCorrigidoSolicitacao = valorCorrigidoSolicitacao;
    }

    public String getAlteracoesSolicitacoes() {
        return alteracoesSolicitacoes;
    }

    public void setAlteracoesSolicitacoes(String alteracoesSolicitacoes) {
        this.alteracoesSolicitacoes = alteracoesSolicitacoes;
    }

    public Candidato getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(Candidato solicitante) {
        this.solicitante = solicitante;
    }

    @Override
    public String toString() {
        return "<html>" 
                + "<p>Data da Solicitação: " + dataSolicitacao + "</p>"
                + "<p>Solicitante: " + solicitante.getNome() + "</p>"
                + "<p>Telefone: " + solicitante.getTelefone() + "</p>"
                + "<p>Email: " + solicitante.getEmail() + "</p>"
                + "<p>Status: " + statusSolicitacao + "</p>"
                + "<p>Valor da Solicitacao: " + valorSolicitacao + "</p>"
                + "<p>Valor Corrigido: " + valorCorrigidoSolicitacao + "</p>"
                + "<p>Data da Visita: " + dataVisitaResidencia + "</p>"
                + "<br>" + "</br>"
                + "</html>";
    }
}
