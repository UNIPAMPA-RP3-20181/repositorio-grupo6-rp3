
package Model;

import java.util.Calendar;

public class ItemServico {
    
    private Calendar dataSolicitacao;
    private String descricaoSolicitacao, descricaoAtendimento, descricaoRecusa;
    private boolean aceitaSolicitacao;
    private Calendar dataAtendimento;

    public ItemServico(Calendar dataSolicitacao, String descricaoSolicitacao, String descricaoAtendimento, String descricaoRecusa, boolean aceitaSolicitacao, Calendar dataAtendimento) {
        this.dataSolicitacao = dataSolicitacao;
        this.descricaoSolicitacao = descricaoSolicitacao;
        this.descricaoAtendimento = descricaoAtendimento;
        this.descricaoRecusa = descricaoRecusa;
        this.aceitaSolicitacao = aceitaSolicitacao;
        this.dataAtendimento = dataAtendimento;
    }

    public Calendar getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(Calendar dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public String getDescricaoSolicitacao() {
        return descricaoSolicitacao;
    }

    public void setDescricaoSolicitacao(String descricaoSolicitacao) {
        this.descricaoSolicitacao = descricaoSolicitacao;
    }

    public String getDescricaoAtendimento() {
        return descricaoAtendimento;
    }

    public void setDescricaoAtendimento(String descricaoAtendimento) {
        this.descricaoAtendimento = descricaoAtendimento;
    }

    public String getDescricaoRecusa() {
        return descricaoRecusa;
    }

    public void setDescricaoRecusa(String descricaoRecusa) {
        this.descricaoRecusa = descricaoRecusa;
    }

    public boolean isAceitaSolicitacao() {
        return aceitaSolicitacao;
    }

    public void setAceitaSolicitacao(boolean aceitaSolicitacao) {
        this.aceitaSolicitacao = aceitaSolicitacao;
    }

    public Calendar getDataAtendimento() {
        return dataAtendimento;
    }

    public void setDataAtendimento(Calendar dataAtendimento) {
        this.dataAtendimento = dataAtendimento;
    }
    
    
    
     
}
