package Model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Residencia {

    private int identificador;
    private int quantidadeComodos, quantidadeBanheiros, quantidadeGaragens;
    private BigDecimal areaConstruida;
    private int nroAndares;
    private String descricaoResidencia;
    private String anoConstrucao;
    private List<Bem> bens = new ArrayList<>();
    private Endereco endereco;
    private BigDecimal areaTotal;

    public Residencia(int quantidadeComodos, int quantidadeBanheiros, int quantidadeGaragens, BigDecimal areaConstruida, BigDecimal areaTotal, int nroAndares, String descricaoResidencia, String anoConstrucao, Endereco endereco) {
        this.quantidadeComodos = quantidadeComodos;
        this.quantidadeBanheiros = quantidadeBanheiros;
        this.quantidadeGaragens = quantidadeGaragens;
        this.areaConstruida = areaConstruida;
        this.areaTotal = areaTotal;
        this.nroAndares = nroAndares;
        this.descricaoResidencia = descricaoResidencia;
        this.anoConstrucao = anoConstrucao;
        this.endereco = endereco;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public BigDecimal getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(BigDecimal areaTotal) {
        this.areaTotal = areaTotal;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Bem> getBens() {
        return bens;
    }

    public void setBens(List<Bem> bens) {
        this.bens = bens;
    }

    public int getQuantidadeComodos() {
        return quantidadeComodos;
    }

    public void setQuantidadeComodos(int quantidadeComodos) {
        this.quantidadeComodos = quantidadeComodos;
    }

    public int getQuantidadeBanheiros() {
        return quantidadeBanheiros;
    }

    public void setQuantidadeBanheiros(int quantidadeBanheiros) {
        this.quantidadeBanheiros = quantidadeBanheiros;
    }

    public int getQuantidadeGaragens() {
        return quantidadeGaragens;
    }

    public void setQuantidadeGaragens(int quantidadeGaragens) {
        this.quantidadeGaragens = quantidadeGaragens;
    }

    public BigDecimal getAreaConstruida() {
        return areaConstruida;
    }

    public void setAreaConstruida(BigDecimal areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    public int getNroAndares() {
        return nroAndares;
    }

    public void setNroAndares(int nroAndares) {
        this.nroAndares = nroAndares;
    }

    public String getDescricaoResidencia() {
        return descricaoResidencia;
    }

    public void setDescricaoResidencia(String descricaoResidencia) {
        this.descricaoResidencia = descricaoResidencia;
    }

    public String getAnoConstrucao() {
        return anoConstrucao;
    }

    public void setAnoConstrucao(String anoConstrucao) {
        this.anoConstrucao = anoConstrucao;
    }

    @Override
    public String toString() {
        return "<html>"
                + "<p>Área Total: " + areaTotal + "</p>"
                + "<p>Área Construida: " + areaConstruida + "</p>"
                + "<p>Ano de Construção: " + anoConstrucao + "</p>"
                + "<p>Número de Andares: " + nroAndares + "</p>"
                + "<p>Quantidade de Comodos: " + quantidadeComodos + "</p>"
                + "<p>Quantidade de Banheiros: " + quantidadeBanheiros + "</p>"
                + "<p>Quantidade de Garagens: " + quantidadeGaragens + "</p>"
                + endereco.toString()
                + "<p>descricaoResidencia: " + descricaoResidencia + "</p>"
                + "<br>" + "</br>"
                + "</html>";
    }

}
