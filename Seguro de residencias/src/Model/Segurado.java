package Model;

public class Segurado extends Candidato {

    private int nroApolices;

    public Segurado(Candidato candidato, int nroApolices) {
        super(candidato.getEmail(), candidato.getSexo(), candidato.getEndereco(), candidato.getNome(), candidato.getCPF(), candidato.getTelefone(), candidato.getDataNascimento(), candidato.getNomeUsuario(), candidato.getSenha());
        this.nroApolices = nroApolices;
    }

    public Segurado(String email, Endereco endereco, int nroApolices, String sexo, String nome, String CPF, String telefone, String dataNascimento, String nomeUsuario, String senha) {
        super(email, sexo, endereco, nome, CPF, telefone, dataNascimento, nomeUsuario, senha);
        this.nroApolices = nroApolices;
    }

    public int getNroApolices() {
        return nroApolices;
    }

    public void setNroApolices(int nroApolices) {
        this.nroApolices = nroApolices;
    }

}
