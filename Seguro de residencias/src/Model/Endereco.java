package Model;

public class Endereco {

    private String cidade;
    private String nomeRua;
    private String nroCasa;
    private String nroCep;
    private String bairro;
    private String estado;

    public Endereco(String estado, String cidade, String nomeRua, String nroCasa, String nroCep, String bairro) {
        this.cidade = cidade;
        this.nomeRua = nomeRua;
        this.nroCasa = nroCasa;
        this.nroCep = nroCep;
        this.bairro = bairro;
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getNomeRua() {
        return nomeRua;
    }

    public void setNomeRua(String nomeRua) {
        this.nomeRua = nomeRua;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public String getNroCep() {
        return nroCep;
    }

    public void setNroCep(String nroCep) {
        this.nroCep = nroCep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    @Override
    public String toString() {
        return "<html>"
                + "<p>CEP: " + nroCep + "</p>"
                + "<p>Estado: " + estado + "</p>"
                + "<p>Cidade: " + cidade + "</p>"
                + "<p>Bairro: " + bairro + "</p>"
                + "<p>Nome da rua: " + nomeRua + "</p>"
                + "<p>Número da casa: " + nroCasa + "</p>"
                + "</html>";
    }

}
