package Model;

public class Bem {

    private String descricaoBem;
    private String valorEstimadoBem;

    public Bem(String descricaoBem, String valorEstimadoBem) {
        this.descricaoBem = descricaoBem;
        this.valorEstimadoBem = valorEstimadoBem;
    }


    public String getDescricaoBem() {
        return descricaoBem;
    }

    public void setDescricaoBem(String descricaoBem) {
        this.descricaoBem = descricaoBem;
    }

    public String getValorEstimadoBem() {
        return valorEstimadoBem;
    }

    public void setValorEstimadoBem(String valorEstimadoBem) {
        this.valorEstimadoBem = valorEstimadoBem;
    }

    @Override
    public String toString() {
        return "<html>"
                + "<p>Valor: " + this.valorEstimadoBem + "</p>"
                + "<p>Descrição: " + this.descricaoBem + "</p>"
                + "</html>";
        

    }

}
