-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16-Jul-2018 às 21:10
-- Versão do servidor: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: dbsafezone
--

-- --------------------------------------------------------

--
-- Estrutura da tabela apoliceseguro
--

CREATE TABLE apoliceseguro (
  idApoliceSeguro int(11) NOT NULL,
  idSoliSeg_Res int(11) NOT NULL,
  idPessoa int(11) NOT NULL,
  nomeNoCartao varchar(45) NOT NULL,
  codigoSegurancaCartao varchar(3) NOT NULL,
  dataContratacaoApolice varchar(10) NOT NULL,
  numeroApolice varchar(45) NOT NULL,
  vencimentoCartao varchar(5) NOT NULL,
  bandeiraCartao varchar(45) NOT NULL,
  premioApolice decimal(10,0) NOT NULL,
  valorParcela decimal(10,0) NOT NULL,
  dataPgtoParcela varchar(10) NOT NULL,
  quantidadeParcelas varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela bens
--

CREATE TABLE bens (
  idBens int(11) NOT NULL,
  idSoliSeg_Res int(11) NOT NULL,
  descricaoBem varchar(180) NOT NULL,
  valoEstimadoBem varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela bens
--

INSERT INTO bens (idBens, idSoliSeg_Res, descricaoBem, valoEstimadoBem) VALUES
(1, 1, 'Carro preto', '203,30'),
(2, 1, 'Cadeira rosa', '130,10');

-- --------------------------------------------------------

--
-- Estrutura da tabela cand_corretor_seg
--

CREATE TABLE cand_corretor_seg (
  idPessoa int(11) NOT NULL,
  nivelPermissao int(1) NOT NULL,
  nome varchar(210) NOT NULL,
  nomeUsuario varchar(45) NOT NULL,
  senha varchar(45) NOT NULL,
  dataDeNascimento varchar(10) NOT NULL,
  cpf varchar(14) NOT NULL,
  sexoCandSeg varchar(1) DEFAULT NULL,
  ativoCorretor tinyint(4) DEFAULT NULL,
  dataContratacao varchar(10) DEFAULT NULL,
  numeroDeApolices int(11) DEFAULT NULL,
  cidade varchar(30) NOT NULL,
  estado varchar(30) NOT NULL,
  bairro varchar(45) NOT NULL,
  rua varchar(45) NOT NULL,
  nroCasa varchar(30) NOT NULL,
  telefone varchar(15) NOT NULL,
  email varchar(100) NOT NULL COMMENT 'sandro-matheus@hotmail.com',
  cep varchar(9) NOT NULL COMMENT '97543-410'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela cand_corretor_seg
--

INSERT INTO cand_corretor_seg (idPessoa, nivelPermissao, nome, nomeUsuario, senha, dataDeNascimento, cpf, sexoCandSeg, ativoCorretor, dataContratacao, numeroDeApolices, cidade, estado, bairro, rua, nroCasa, telefone, email, cep) VALUES
(1, 1, 'M', 'M', '1', '10061999', '025433432', 'M', NULL, NULL, NULL, 'Alegrete', 'RS', 'KK', 'KKKK', '32', '2321321312', 'DSFSDFDSFSFDSF', '213213'),
(2, 3, 'Uziel', 'Uziel', '1', '10061999', '02536653028', NULL, 1, '10062018', NULL, 'alegrete', 'rs', 'maximinio marinho', 'sei lá', '322', '55997328105', 'sandro-matheus@hotmail.com', '97543410');

-- --------------------------------------------------------

--
-- Estrutura da tabela sinistro
--

CREATE TABLE sinistro (
  idSinistro int(11) NOT NULL,
  idTipoSinistro int(11) NOT NULL,
  idAvaliador int(11) NOT NULL,
  idApoliceSeguro int(11) NOT NULL,
  valorSinistro decimal(10,0) NOT NULL,
  dataSinistro varchar(10) NOT NULL,
  parecerAvaliador varchar(10) NOT NULL,
  descricaoSinistro varchar(180) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela solicitacaoservico
--

CREATE TABLE solicitacaoservico (
  idSolicitacaoServico int(11) NOT NULL,
  idApoliceSeguro int(11) NOT NULL,
  idAvaliador int(11) NOT NULL,
  descricaoSolicitacao varchar(180) NOT NULL,
  statusSolicitacao varchar(30) NOT NULL,
  descricaoRecusa varchar(180) DEFAULT NULL,
  dataSolicitacao varchar(10) NOT NULL,
  descricaoAtendimento varchar(180) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela soliseg_res
--

CREATE TABLE soliseg_res (
  idSoliSeg_Res int(11) NOT NULL,
  idPessoa int(11) NOT NULL,
  dataVisitaResidencia varchar(10) NOT NULL,
  valorCorigidoSolicitacao decimal(10,0) DEFAULT NULL,
  motivoReprovacao varchar(180) DEFAULT NULL,
  terrenoPerigoso int(2) NOT NULL,
  estruturaAmeacada int(2) NOT NULL,
  localizacaoPerigosa int(2) NOT NULL,
  statusSolicitacao varchar(45) NOT NULL,
  alteracoesSolicitacao varchar(45) DEFAULT NULL,
  dataSolicitacao varchar(45) NOT NULL,
  valorSolicitacao decimal(10,0) NOT NULL,
  quantComodosRes int(3) NOT NULL,
  quantBanheirosRes int(3) NOT NULL,
  quantGaragensRes int(3) NOT NULL,
  nroAndaresRes int(3) NOT NULL,
  nroCasaRes varchar(5) NOT NULL,
  cepRes varchar(9) NOT NULL,
  ruaRes varchar(45) NOT NULL,
  cidadeRes varchar(30) NOT NULL,
  estadoRes varchar(30) NOT NULL,
  bairroRes varchar(45) NOT NULL,
  areaConstruidaRes decimal(10,0) NOT NULL,
  areaTotalRes decimal(10,0) NOT NULL,
  anoConstrucaoRes varchar(10) NOT NULL,
  descricaoResidencia varchar(180) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela soliseg_res
--

INSERT INTO soliseg_res (idSoliSeg_Res, idPessoa, dataVisitaResidencia, valorCorigidoSolicitacao, motivoReprovacao, terrenoPerigoso, estruturaAmeacada, localizacaoPerigosa, statusSolicitacao, alteracoesSolicitacao, dataSolicitacao, valorSolicitacao, quantComodosRes, quantBanheirosRes, quantGaragensRes, nroAndaresRes, nroCasaRes, cepRes, ruaRes, cidadeRes, estadoRes, bairroRes, areaConstruidaRes, areaTotalRes, anoConstrucaoRes, descricaoResidencia) VALUES
(1, 1, '', '0', 'SEILA VELHO', 3, 2, 2, 'Recusado', NULL, '10/06/2018', '2323', 2, 2, 3, 1, '231', '97543410', 'alegrete', 'alegrete', 'teste', 'teste', '3232', '3232', '10061999', 'uma casa ');

-- --------------------------------------------------------

--
-- Estrutura da tabela tiposinistro
--

CREATE TABLE tiposinistro (
  idTipoSinistro int(11) NOT NULL,
  descricaoTipoSinistro varchar(180) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table apoliceseguro
--
ALTER TABLE apoliceseguro
  ADD PRIMARY KEY (idApoliceSeguro,idSoliSeg_Res,idPessoa),
  ADD KEY fk_ApoliceSeguro_SoliSeg_Res1_idx (idSoliSeg_Res,idPessoa);

--
-- Indexes for table bens
--
ALTER TABLE bens
  ADD PRIMARY KEY (idBens,idSoliSeg_Res),
  ADD KEY fk_Bens_SoliSeg_Res_idx (idSoliSeg_Res);

--
-- Indexes for table cand_corretor_seg
--
ALTER TABLE cand_corretor_seg
  ADD PRIMARY KEY (idPessoa),
  ADD UNIQUE KEY cpf_UNIQUE (cpf),
  ADD UNIQUE KEY nomeUsuario_UNIQUE (nomeUsuario);

--
-- Indexes for table sinistro
--
ALTER TABLE sinistro
  ADD PRIMARY KEY (idSinistro,idTipoSinistro,idAvaliador,idApoliceSeguro),
  ADD KEY fk_Sinistro_TipoSinistro1_idx (idTipoSinistro),
  ADD KEY fk_Sinistro_Pessoas_Cand_Seg_Corr1_idx (idAvaliador),
  ADD KEY fk_Sinistro_ApoliceSeguro1_idx (idApoliceSeguro);

--
-- Indexes for table solicitacaoservico
--
ALTER TABLE solicitacaoservico
  ADD PRIMARY KEY (idSolicitacaoServico,idApoliceSeguro,idAvaliador),
  ADD KEY fk_SolicitacaoServico_ApoliceSeguro1_idx (idApoliceSeguro),
  ADD KEY fk_SolicitacaoServico_Cand_Corretor_Seg1_idx (idAvaliador);

--
-- Indexes for table soliseg_res
--
ALTER TABLE soliseg_res
  ADD PRIMARY KEY (idSoliSeg_Res,idPessoa),
  ADD KEY fk_SoliSeg_Res_Pessoas_Cand_Seg1_idx (idPessoa);

--
-- Indexes for table tiposinistro
--
ALTER TABLE tiposinistro
  ADD PRIMARY KEY (idTipoSinistro);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table bens
--
ALTER TABLE bens
  MODIFY idBens int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table cand_corretor_seg
--
ALTER TABLE cand_corretor_seg
  MODIFY idPessoa int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table sinistro
--
ALTER TABLE sinistro
  MODIFY idSinistro int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table soliseg_res
--
ALTER TABLE soliseg_res
  MODIFY idSoliSeg_Res int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela apoliceseguro
--
ALTER TABLE apoliceseguro
  ADD CONSTRAINT fk_ApoliceSeguro_SoliSeg_Res1 FOREIGN KEY (idSoliSeg_Res,idPessoa) REFERENCES soliseg_res (idSoliSeg_Res, idPessoa) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela bens
--
ALTER TABLE bens
  ADD CONSTRAINT fk_Bens_SoliSeg_Res FOREIGN KEY (idSoliSeg_Res) REFERENCES soliseg_res (idSoliSeg_Res) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela sinistro
--
ALTER TABLE sinistro
  ADD CONSTRAINT fk_Sinistro_ApoliceSeguro1 FOREIGN KEY (idApoliceSeguro) REFERENCES apoliceseguro (idApoliceSeguro) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Sinistro_Pessoas_Cand_Seg_Corr1 FOREIGN KEY (idAvaliador) REFERENCES cand_corretor_seg (idPessoa) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Sinistro_TipoSinistro1 FOREIGN KEY (idTipoSinistro) REFERENCES tiposinistro (idTipoSinistro) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela solicitacaoservico
--
ALTER TABLE solicitacaoservico
  ADD CONSTRAINT fk_SolicitacaoServico_ApoliceSeguro1 FOREIGN KEY (idApoliceSeguro) REFERENCES apoliceseguro (idApoliceSeguro) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_SolicitacaoServico_Cand_Corretor_Seg1 FOREIGN KEY (idAvaliador) REFERENCES cand_corretor_seg (idPessoa) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela soliseg_res
--
ALTER TABLE soliseg_res
  ADD CONSTRAINT fk_SoliSeg_Res_Pessoas_Cand_Seg1 FOREIGN KEY (idPessoa) REFERENCES cand_corretor_seg (idPessoa) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
